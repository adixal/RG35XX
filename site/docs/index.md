---
title: muOS Introduction
description: So you've decided to try out the muOS custom firmware for the Anbernic RG35XX handheld.
hide:
  - navigation
  - toc
---

![muOS Logo](assets/muos.jpg){ width="256" }

# muOS

So you've decided to try out the muOS custom firmware for the various Anbernic handhelds.  This is a personal hobby project of mine and I hope that you'll enjoy it too.  I love RetroArch and its simplicity to get my favourite games up and running with little effort.  I'm hoping that all the development I put into this make it as enjoyable as I have had creating it.

## Features
One of the main features of muOS is how simple it is to get it set up and running, just like other CFWs all you have to do is use your favourite image flash program to put it on your SD Card (don't use the stock SD card!).  Put the SD Card into your handheld, turn it on, and it will take care of the rest by initialising the free space on your card for your ROMs.  There are several pre-installed games and homebrew that you can start playing straight away, and if you're not sure what to play you can always choose the shuffle option to let the device pick for you!

## Screenshots
![muOS Main Menu](assets/mainmenu.png){ width="320" }
![Game Picker](assets/game.png){ width="320" }
![Game Info](assets/info.png){ width="320" }
![Game Preview](assets/preview.png){ width="320" }
![Game Preview](assets/fauxdark.png){ width="320" }
![Game Preview](assets/fallout.png){ width="320" }
![Game Preview](assets/dott.png){ width="320" }
![Game Preview](assets/sysinfo.png){ width="320" }

## Global System Controls
| Type       | Controls                             |
| ---------- | ------------------------------------ |
| Audio Mute | `SELECT` + `DPAD DOWN`               |
| Brightness | `SELECT` + `VOLUP` or `VOLDOWN`      |
| Screenshot | Hold `VOLUP` then press `POWER` once |
| Sleep Mode | Tap `POWER` once to sleep or wake    |

## Download latest muOS
Download the latest image from the muOS discord server. You can also become a tester and preview exciting new features that are a work in progress! muOS is open to all kinds of suggestions, fixes, and bug reports.

[:simple-discord: Click to join Discord server](https://discord.gg/USS5ybVtDz){ .md-button .md-button--primary }

## Support muOS
If you like using muOS and would like to see it grow, and potentially support more devices, feel free to give a small tip!

[:simple-kofi: Support muOS via Ko-fi](https://ko-fi.com/xonglebongle){ .md-button .md-button--primary }
