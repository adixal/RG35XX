#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

#define BUFFER_SIZE 8192

Mix_Music *background_music = NULL;
Mix_Chunk *current_sound = NULL;

void play_sound(const char *filename) {
    Mix_Chunk *sound = Mix_LoadWAV(filename);

    if (current_sound != NULL) {
        Mix_HaltChannel(-1);
        Mix_FreeChunk(current_sound);
    }

    Mix_PlayChannel(-1, sound, 0);
    current_sound = sound;

    while (Mix_Playing(-1)) {
        SDL_Delay(64);
    }

    Mix_FreeChunk(current_sound);
    current_sound = NULL;
}

void play_background_music(const char *filename) {
    background_music = Mix_LoadMUS(filename);

    if (!background_music) {
        perror("Failed to load background music");
        return;
    }

    Mix_PlayMusic(background_music, -1);
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <named pipe>\n", argv[0]);
        return EXIT_FAILURE;
    }

    const char *pipePath = argv[1];
    int pipe_fd = open(pipePath, O_RDONLY);

    if (pipe_fd == -1) {
        perror("Failed to open named pipe");
        return EXIT_FAILURE;
    }

    SDL_Init(SDL_INIT_AUDIO);
    Mix_Init(MIX_INIT_MP3);

    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024) == -1) {
        perror("Failed to open audio device");
        return EXIT_FAILURE;
    }

    play_background_music("/opt/muos/silence.mp3");

    while (1) {
        char command[BUFFER_SIZE];
        ssize_t bytes_pipe = read(pipe_fd, command, sizeof(command) - 1);

        if (bytes_pipe == -1) {
            perror("Error reading from named pipe");
            break;
        } else if (bytes_pipe == 0) {
            continue;
        }

        command[bytes_pipe] = '\0';
        char *newline = strchr(command, '\n');
        if (newline != NULL) {
            *newline = '\0';
        }

        if (strcmp(command, "quit") == 0) {
            break;
        } else {
            char sound_path[BUFFER_SIZE];
            snprintf(sound_path, sizeof(sound_path), "/opt/muos/theme/sound/%s.mp3", command);
            play_sound(sound_path);
        }
    }

    Mix_FreeMusic(background_music);
    Mix_CloseAudio();
    SDL_Quit();
    close(pipe_fd);

    return EXIT_SUCCESS;
}