#include <fcntl.h>
#include <linux/fb.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>

int open_framebuffer_device() {
    int fbfd = open("/dev/fb0", O_RDWR);
    if (fbfd == -1) {
        perror("Error opening framebuffer device");
        return -1;
    }
    return fbfd;
}

int get_fix_screeninfo(int fbfd, struct fb_fix_screeninfo* finfo) {
    if (ioctl(fbfd, FBIOGET_FSCREENINFO, finfo) == -1) {
        perror("Error reading fixed information");
        close(fbfd);
        return -1;
    }
    return 0;
}

int get_var_screeninfo(int fbfd, struct fb_var_screeninfo* vinfo) {
    if (ioctl(fbfd, FBIOGET_VSCREENINFO, vinfo) == -1) {
        perror("Error reading variable information");
        close(fbfd);
        return -1;
    }
    return 0;
}

int set_screen_resolution(int fbfd, struct fb_var_screeninfo* vinfo, int xres, int yres) {
    vinfo->xres = xres;
    vinfo->yres = yres;
    if (ioctl(fbfd, FBIOPUT_VSCREENINFO, vinfo) == -1) {
        perror("Error setting variable information");
        close(fbfd);
        return -1;
    }
    return 0;
}

char* map_framebuffer_memory(int fbfd, struct fb_fix_screeninfo* finfo) {
    char* fbp = (char*)mmap(0, finfo->smem_len, PROT_READ | PROT_WRITE, MAP_SHARED, fbfd, 0);
    if (fbp == MAP_FAILED) {
        perror("Error mapping framebuffer");
        close(fbfd);
        return NULL;
    }
    return fbp;
}

void draw_bar(unsigned int barWidth, unsigned int startPos,
              unsigned short colourHex, unsigned int showTime) {
    int    fbfd = open_framebuffer_device();
    struct fb_fix_screeninfo finfo;
    struct fb_var_screeninfo vinfo;

    get_fix_screeninfo(fbfd, &finfo);
    get_var_screeninfo(fbfd, &vinfo);
    set_screen_resolution(fbfd, &vinfo, 320, 240);

    char* fbp = map_framebuffer_memory(fbfd, &finfo);

    unsigned int barX = 0;
    unsigned int barY = (vinfo.yres * startPos) / 100;

    for (unsigned int i = 0; i < showTime; ++i) {
        for (unsigned int x = barX; x < barX + barWidth; ++x) {
            for (unsigned int y = barY; y < vinfo.yres; ++y) {
                long location = (x + vinfo.xoffset) * (vinfo.bits_per_pixel / 8) +
                                (y + vinfo.yoffset) * vinfo.xres_virtual *
                                (vinfo.bits_per_pixel / 8);

                unsigned short* pixel = (unsigned short*)(fbp + location);
                *pixel = colourHex;
            }
        }
    }

    munmap(fbp, finfo.smem_len);
    close(fbfd);
}
