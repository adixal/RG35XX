#ifndef FB_DRAWBAR
#define FB_DRAWBAR

#include <fcntl.h>
#include <linux/fb.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>

int open_framebuffer_device();

int get_fix_screeninfo(int fbfd, struct fb_fix_screeninfo* finfo);
int get_var_screeninfo(int fbfd, struct fb_var_screeninfo* vinfo);
int set_screen_resolution(int fbfd, struct fb_var_screeninfo* vinfo, int xres, int yres);

char* map_framebuffer_memory(int fbfd, struct fb_fix_screeninfo* finfo);

void draw_bar(unsigned int barWidth, unsigned int startPos,
              unsigned short colourHex, unsigned int showTime);

#endif
