#!/bin/sh

PORTS_FOLDER=$(realpath "$(dirname "$0")")
GAMEDIR="$PORTS_FOLDER/.Dingux Commander"

chmod +x "$GAMEDIR"/DinguxCommander
cd "$GAMEDIR" || exit

HOME="$GAMEDIR" SDL_ASSERT=always_ignore SDL_GAMECONTROLLERCONFIG=$(grep "Deeplay" "$GAMEDIR/gamecontrollerdb.txt") ./DinguxCommander --config "$GAMEDIR/commander.cfg"

