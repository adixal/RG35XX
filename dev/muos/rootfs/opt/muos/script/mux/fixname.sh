#!/bin/sh

if [ -z "$1" ]; then
    echo "Usage: $0 <filename>"
    exit 1
fi

FILENAME="$1"

if [ ! -f "$FILENAME" ]; then
    echo "File not found: $FILENAME"
    exit 1
fi

INVALID='*?.,;:/\|+=<>[]"'
PLACEHOLDER='\^'

sed -i "s/=/$PLACEHOLDER/g" "$FILENAME"
tr  -d "[$INVALID]" < "$FILENAME" > "${FILENAME}_temp"
mv     "${FILENAME}_temp"   "$FILENAME"
sed -i "s/$PLACEHOLDER/=/1" "$FILENAME"

