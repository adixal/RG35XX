#!/bin/sh

TWEAK=/opt/muos/config/tweak.txt
. $TWEAK

THEMEDIR="/opt/muos/theme"
BOOTLOGO="$THEMEDIR/image/bootlogo.bmp"

cp "/opt/muos/backup/bootlogo.bmp" "/mnt/boot/bootlogo.bmp"

rm -rf "$THEMEDIR"
unzip "/mnt/mmc/MUOS/info/theme/$THEME" -d "$THEMEDIR"

if [ -f "$BOOTLOGO" ]; then
	cp "$BOOTLOGO" "/mnt/boot/bootlogo.bmp"
fi

REFRESH="mushot"

for PROG in $REFRESH; do
	PROG_PATH="/opt/muos/bin/$PROG"
	if [ -x "$PROG_PATH" ]; then
		if [ "$PROG" = "mushot" ]; then
			ARGS="$SCREENSHOT"
		else
			ARGS=""
		fi
		if ! pgrep "$PROG" > /dev/null; then
			"$PROG_PATH" "$ARGS" &
		fi
	fi
done

if [ "$NIGHT" = true ]; then
	echo 255 > /sys/class/disp/disp/attr/color_temperature
else
	echo 0 > /sys/class/disp/disp/attr/color_temperature
fi

sync

