#!/bin/sh

BACKUP=/opt/muos/backup
CONFIG=/opt/muos/config

FILES="brightness.txt lastplay.txt network.txt volume.txt"

for F in $FILES; do
    if [ -e "$CONFIG/$F" ]; then
        if [ ! -s "$CONFIG/$F" ]; then
            cp "$BACKUP/$F" "$CONFIG/$F"
        fi
    else
        cp "$BACKUP/$F" "$CONFIG/$F"
    fi
done

