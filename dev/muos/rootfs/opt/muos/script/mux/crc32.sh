#!/bin/sh

if [ "$#" -ne 2 ]; then
	echo "Usage: $0 <ROM PATH> <CACHE PATH>"
	exit 1
fi

RMUX=/tmp/r_file.mux
CMUX=/tmp/c_file.mux

C32() {
	crc32 "$1" | awk '{print $1}'
}

FLIST() {
	find "$1" -maxdepth 1 -type f -exec basename {} \; | sed 's/\.[^.]*$//' | sort
}

mkdir -p "$1"
mkdir -p "$2"

FLIST "$1" > "$RMUX"
FLIST "$2" > "$CMUX"

if [ "$(C32 $RMUX)" = "$(C32 $CMUX)" ]; then
	printf 1
else
	printf 0
fi

rm $RMUX $CF

