#!/bin/sh

NAME=$1
CORE=$2
ROM=$3

ROMPATH=$(echo "$ROM" | awk -F'/' '{NF--; print}' OFS='/')
ERPC=$(<"$ROM.cfg" sed 's/[[:space:]]*$//')

if [ -d "$ROMPATH/.$NAME" ]; then
	SUBFOLDER=".$NAME"
else
	SUBFOLDER="$NAME"
fi

/opt/muos/script/mux/track.sh "$NAME" /mnt/mmc/MUOS/retroarch -c \""/mnt/mmc/MUOS/.retroarch/retroarch.cfg"\" -L \""/mnt/mmc/MUOS/core/easyrpg_libretro.so"\" \""$ROMPATH/$SUBFOLDER/$ERPC"\"

