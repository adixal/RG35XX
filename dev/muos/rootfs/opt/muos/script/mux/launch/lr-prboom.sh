#!/bin/sh

NAME=$1
CORE=$2
ROM=$3

ROMPATH=$(echo "$ROM" | awk -F'/' '{NF--; print}' OFS='/')

mkdir -p "$ROMPATH/$NAME"

PRBC="$ROMPATH/$NAME/prboom.cfg"
IWAD=$(awk -F'"' '/parentwad/ {print $2}' "$ROMPATH/$NAME.doom")

cp "$ROMPATH/$NAME.doom" "$PRBC"
cp /mnt/mmc/MUOS/bios/prboom.wad "$ROMPATH/$NAME/prboom.wad"
cp "$ROMPATH/.IWAD/$IWAD" "$ROMPATH/$NAME/$IWAD"

/opt/muos/script/mux/track.sh "$NAME" /mnt/mmc/MUOS/retroarch -c \""/mnt/mmc/MUOS/.retroarch/retroarch.cfg"\" -L \""/mnt/mmc/MUOS/core/prboom_libretro.so"\" \""$ROMPATH/$NAME/$IWAD"\"

rm -rf "${ROMPATH:?}/$NAME"

