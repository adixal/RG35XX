#!/bin/sh

if [ "$#" -ne 1 ]; then
	echo "Usage: $0 <input>"
	exit 1
fi

awk '
BEGIN { RS = " "; ORS = ""; }

{
	if (length(line $0) > 40) {
		print line "\n";
		line = $0 " ";
	} else {
		line = line $0 " ";
	}
}

END {
	if (length(line) > 40) {
		print substr(line, 1, 40) "\n" substr(line, 41) "\n";
	} else {
		print line "\n";
	}
}
' "$1"

