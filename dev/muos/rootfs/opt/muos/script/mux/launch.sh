#!/bin/sh
# shellcheck disable=1090,2002

TWEAK=/opt/muos/config/tweak.txt
. $TWEAK

ACT_GO=/tmp/act_go
ASS_GO=/tmp/ass_go
ROM_GO=/tmp/rom_go
SYS_GO=/tmp/sys_go

EX_CARD=/tmp/explore_card

ROM_LAST=/tmp/rom_last

SND_PIPE=/tmp/muplay_pipe

echo "$STARTUP" > $ACT_GO
echo "root" > $EX_CARD

CARD=""
SYSTEM=""
LAST_PLAY="/opt/muos/config/lastplay.txt"

GPTOKEYB_BIN=gptokeyb
GPTOKEYB_DIR=/mnt/mmc/MUOS/emulator/gptokeyb
GPTOKEYB_CONTROLLERCONFIG="$GPTOKEYB_DIR/gamecontrollerdb.txt"
GPTOKEYB_CONFDIR=/opt/muos/config/gptokeyb

# Needs to be exported as evsieve .sh scripts use same vars
export EVSIEVE_BIN=evsieve
export EVSIEVE_DIR=/opt/muos/bin
EVSIEVE_CONFDIR=/opt/muos/config/evsieve

BGM_PID=""

LOGGER() {
if [ "$VERBOSE" = true ]; then
	_TITLE=$1
	_MESSAGE=$2
	_FORM=$(cat <<EOF
$_TITLE

$_MESSAGE
EOF
	)
	/opt/muos/extra/muxstart "$_FORM" && sleep 0.5
fi
}

KILL_BGM() {
	if [ -n "$BGM_PID" ]; then
		kill "$BGM_PID"
		BGM_PID=""
	fi
	pkill "mp3play"
	pkill "playbgm.sh"
}

KILL_SND() {
	echo "quit" > "$SND_PIPE"
	pkill "muplay"
	rm "$SND_PIPE"
}

if [ "$STARTUP" = last ]; then
	cat $LAST_PLAY > $ROM_GO
fi

while true; do
	. $TWEAK

	# Background Music
	if [ "$MSOUND" = bgm ]; then
		if ! pgrep "playbgm.sh" > /dev/null; then
			/opt/muos/script/mux/playbgm.sh
			BGM_PID=$!
		fi
	else
		KILL_BGM
	fi

	# Navigation Sounds
	if [ "$MSOUND" = nav ]; then
		if ! pgrep "muplay" > /dev/null; then
			mkfifo "$SND_PIPE"
			/opt/muos/bin/muplay "$SND_PIPE" &
		fi
	else
		KILL_SND
	fi

	# Core Association
	if [ -s "$ASS_GO" ]; then
		ROM_DIR=$(cat "$ASS_GO" | sed -n '1p')
		ROM_SYS=$(cat "$ASS_GO" | sed -n '2p')

		rm "$ASS_GO"
		echo "assign" > $ACT_GO
	fi

	# Content Loader
	if [ -s "$ROM_GO" ]; then
		cat $ROM_GO > $ROM_LAST

		NAME=$(cat "$ROM_GO" | sed -n '1p')
		CORE=$(cat "$ROM_GO" | sed -n '2p' | tr -d '\n')
		R_DIR=$(cat "$ROM_GO" | sed -n '4p')$(cat "$ROM_GO" | sed -n '5p')
		ROM="$R_DIR"/$(cat "$ROM_GO" | sed -n '6p')

		printf "TRYING TO LAUNCH: %s\n" "$ROM"
		printf "USING CORE: %s\n" "$CORE"

		rm "$ROM_GO"

		if [ -f "$GPTOKEYB_CONFDIR/$CORE.gptk" ]; then
			SDL_GAMECONTROLLERCONFIG_FILE="$GPTOKEYB_CONTROLLERCONFIG" \
			"$GPTOKEYB_DIR/$GPTOKEYB_BIN" -c "$GPTOKEYB_CONFDIR/$CORE.gptk" &
		fi

		if [ -f "$EVSIEVE_CONFDIR/$CORE.evs.sh" ]; then
			"$EVSIEVE_CONFDIR/$CORE.evs.sh"
		fi

		if [ "$MSOUND" = bgm ]; then
			KILL_BGM
			sleep 1
		fi
		if [ "$MSOUND" = nav ]; then
			KILL_SND
			sleep 1
		fi

		# External Script
		if [ "$CORE" = external ]; then
			/opt/muos/script/mux/launch/ext-general.sh "$NAME" "$CORE" "$ROM"
		# PICO-8 External
		elif [ "$CORE" = ext-pico8 ]; then
			/opt/muos/script/mux/launch/ext-pico8.sh "$NAME" "$CORE" "$ROM"
		# DraStic External
		elif [ "$CORE" = ext-drastic ]; then
			/opt/muos/script/mux/launch/ext-drastic.sh "$NAME" "$CORE" "$ROM"
		# ScummVM External
		elif [ "$CORE" = ext-scummvm ]; then
			/opt/muos/script/mux/launch/ext-scummvm.sh "$NAME" "$CORE" "$ROM"
		# ScummVM LibRetro
		elif [ "$CORE" = scummvm_libretro.so ]; then
			/opt/muos/script/mux/launch/lr-scummvm.sh "$NAME" "$CORE" "$ROM"
		# PrBoom LibRetro
		elif [ "$CORE" = prboom_libretro.so ]; then
			/opt/muos/script/mux/launch/lr-prboom.sh "$NAME" "$CORE" "$ROM"
		# EasyRPG LibRetro
		elif [ "$CORE" = easyrpg_libretro.so ]; then
			/opt/muos/script/mux/launch/lr-easyrpg.sh "$NAME" "$CORE" "$ROM"
		# Standard LibRetro
		else
			/opt/muos/script/mux/launch/lr-general.sh "$NAME" "$CORE" "$ROM"
		fi

		echo explore > $ACT_GO

		# Do it twice, it's just as nice!
		cat /dev/zero > /dev/fb0 2>/dev/null
		cat /dev/zero > /dev/fb0 2>/dev/null

		cat $ROM_LAST > $LAST_PLAY
		[ "$(cat $ACT_GO)" = last ] && echo launcher > $ACT_GO

		killall $GPTOKEYB_BIN
		killall $EVSIEVE_BIN

		continue
	fi

	# Message Suppression
	if [ -s "/tmp/mux_suppress" ]; then
		MSG_SUPPRESS=$(cat "/tmp/mux_suppress")
		rm "/tmp/mux_suppress"
	else
		MSG_SUPPRESS=0
	fi

	# Get Last ROM Index
	if [ "$(cat $ACT_GO)" = explore ] || [ "$(cat $ACT_GO)" = favourite ] || [ "$(cat $ACT_GO)" = history ]; then
		if [ -s "/tmp/mux_lastindex_rom" ]; then
			LAST_INDEX_ROM=$(cat "/tmp/mux_lastindex_rom")
			rm "/tmp/mux_lastindex_rom"
		else
			LAST_INDEX_ROM=0
		fi
	fi

	# Get Last System Index
	if [ "$(cat $ACT_GO)" = syslist ] && [ -s "/tmp/mux_lastindex_sys" ]; then
		LAST_INDEX_SYS=$(cat "/tmp/mux_lastindex_sys")
		rm "/tmp/mux_lastindex_sys"
	else
		LAST_INDEX_SYS=0
	fi

	# Kill PortMaster GPTOKEYB just in case!
	killall gptokeyb.armhf &

	# muX Programs
	if [ -s "$ACT_GO" ]; then
		case "$(cat $ACT_GO)" in
			"launcher")
				echo launcher > $ACT_GO
				nice --20 /opt/muos/extra/muxlaunch
				;;
			"assign")
				echo explore > $ACT_GO
				echo "$LAST_INDEX_SYS" > /tmp/lisys
				nice --20 /opt/muos/extra/muxassign -d "$ROM_DIR" -s "$ROM_SYS"
				;;
			"explore")
				MODULE=$(cat "$EX_CARD" | sed -n '1p')
				echo launcher > $ACT_GO
				echo "$LAST_INDEX_SYS" > /tmp/lisys
				nice --20 /opt/muos/extra/muxplore -i "$LAST_INDEX_ROM" -m "$MODULE"
				;;
			"config")
				echo launcher > $ACT_GO
				nice --20 /opt/muos/extra/muxconfig
				;;
			"info")
				echo launcher > $ACT_GO
				nice --20 /opt/muos/extra/muxinfo
				;;
			"tweakgen")
				echo config > $ACT_GO
				nice --20 /opt/muos/extra/muxtweakgen
				;;
			"tweakadv")
				echo config > $ACT_GO
				nice --20 /opt/muos/extra/muxtweakadv
				;;
			"theme")
				echo config > $ACT_GO
				nice --20 /opt/muos/extra/muxtheme
				;;
			"network")
				echo config > $ACT_GO
				nice --20 /opt/muos/extra/muxnetwork
				;;
			"webserv")
				echo config > $ACT_GO
				nice --20 /opt/muos/extra/muxwebserv
				;;
			"rtc")
				echo config > $ACT_GO
				nice --20 /opt/muos/extra/muxrtc
				;;
			"timezone")
				echo rtc > $ACT_GO
				nice --20 /opt/muos/extra/muxtimezone
				;;
			"import")
				echo config > $ACT_GO
				nice --20 /opt/muos/extra/muximport
				;;
			"tracker")
				echo info > $ACT_GO
				nice --20 /opt/muos/extra/muxtracker -m "$MSG_SUPPRESS"
				if [ -s "/tmp/mux_reload" ]; then
					if [ "$(cat /tmp/mux_reload)" = 1 ]; then
						echo tracker > $ACT_GO
					fi
					rm "/tmp/mux_reload"
				fi
				;;
			"sdcard")
				echo config > $ACT_GO
				nice --20 /opt/muos/extra/muxsdtool
				;;
			"tester")
				echo info > $ACT_GO
				nice --20 /opt/muos/extra/muxtester
				;;
			"bios")
				echo config > $ACT_GO
				nice --20 /opt/muos/extra/muxbioscheck
				;;
			"backup")
				echo config > $ACT_GO
				nice --20 /opt/muos/extra/muxbackup
				;;
			"reset")
				echo config > $ACT_GO
				nice --20 /opt/muos/extra/muxreset
				;;
			"device")
				echo config > $ACT_GO
				nice --20 /opt/muos/extra/muxdevice
				;;
			"system")
				echo info > $ACT_GO
				nice --20 /opt/muos/extra/muxsysinfo
				;;
			"profile")
				echo launcher > $ACT_GO
				nice --20 /opt/muos/extra/muxprofile
				;;
			"favourite")
				find /mnt/mmc/MUOS/info/favourite -maxdepth 1 -type f -size 0 -delete
				echo launcher > $ACT_GO
				nice --20 /opt/muos/extra/muxplore -i "$LAST_INDEX_ROM" -m favourite
				if [ -s "/tmp/mux_reload" ]; then
					if [ "$(cat /tmp/mux_reload)" = 1 ]; then
						echo favourite > $ACT_GO
					fi
					rm "/tmp/mux_reload"
				fi
				;;
			"history")
				find /mnt/mmc/MUOS/info/history -maxdepth 1 -type f -size 0 -delete
				echo launcher > $ACT_GO
				nice --20 /opt/muos/extra/muxplore -i 0 -m history
				if [ -s "/tmp/mux_reload" ]; then
					if [ "$(cat /tmp/mux_reload)" = 1 ]; then
						echo history > $ACT_GO
					fi
					rm "/tmp/mux_reload"
				fi
				;;
			"portmaster")
				/opt/muos/extra/muxstart "Starting PortMaster" && sleep 0.5
				if [ "$MSOUND" = bgm ]; then
					KILL_BGM
					sleep 1
				fi
				if [ "$MSOUND" = nav ]; then
					KILL_SND
					sleep 1
				fi
				echo launcher > $ACT_GO
				nice --20 /mnt/mmc/MUOS/PortMaster/PortMaster.sh
				;;
			"retro")
				/opt/muos/extra/muxstart "Starting RetroArch" && sleep 0.5
				if [ "$MSOUND" = bgm ]; then
					KILL_BGM
					sleep 1
				fi
				if [ "$MSOUND" = nav ]; then
					KILL_SND
					sleep 1
				fi
				echo launcher > $ACT_GO
				nice --20 /mnt/mmc/MUOS/retroarch -c "/mnt/mmc/MUOS/.retroarch/retroarch.cfg"
				;;
			"shuffle")
				echo launcher > $ACT_GO
				nice --20 /opt/muos/extra/muxshuffle
				;;
			"credits")
				echo info > $ACT_GO
				nice --20 /opt/muos/extra/muxcredits
				;;
		esac
	fi
done

