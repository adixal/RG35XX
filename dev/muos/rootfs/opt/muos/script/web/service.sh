#!/bin/sh
# shellcheck disable=1090,2002

NETCON=/opt/muos/config/network.txt
. $NETCON

NETADDR=$(cat "/opt/muos/config/address.txt")

killall dropbear
killall filebrowser
killall gotty
killall syncthing

if [ "$SRV_SHELL" = true ]; then
	nice -2 dropbear -ER > /dev/null &
fi

if [ "$SRV_BROWSER" = true ]; then
	nice -2 /opt/muos/app/filebrowser --noauth --disable-exec --disable-preview-resize --disable-thumbnails --img-processors 1 -p 9090 -a "$NETADDR" -r /opt/muos/browse/ -d /opt/muos/app/filebrowser.db > /dev/null &
fi

if [ "$SRV_TERMINAL" = true ]; then
	nice -2 /opt/muos/app/gotty --config /opt/muos/app/gotty-config --width 0 --height 0 /bin/sh > /dev/null &
fi

if [ "$SRV_SYNCTHING" = true ]; then
	nice -2 /opt/muos/app/syncthing serve --home="/mnt/mmc/MUOS/syncthing" --skip-port-probing --gui-address="$NETADDR:7070" --no-browser --no-default-folder > /dev/null &
fi

if [ "$SRV_NTP" = true ]; then
	nice -2 ntpdate -b "$NTP_POOL" > /dev/null &
fi

