#!/bin/sh
# shellcheck disable=1090,2002

MUOSBOOT_LOG=/mnt/mmc/MUOS/log/network.txt

TWEAK=/opt/muos/config/tweak.txt
. $TWEAK

NETCON=/opt/muos/config/network.txt
. $NETCON

CURRENT_DATE=$(date +"%Y_%m_%d__%H_%M_%S")

CIP=/opt/muos/config/address.txt

LOGGER() {
if [ "$VERBOSE" = true ]; then
	_MESSAGE=$1
	echo "=== ${CURRENT_DATE} === $_MESSAGE" >> "$MUOSBOOT_LOG"
fi
}

LOGGER "Bringing Wi-Fi Interface Down"
ip link set "$NET_INTERFACE" down

LOGGER "Killing running web services"
killall dropbear
killall filebrowser
killall gotty
killall syncthing

echo "No IP Address" | tr -d '\n' > "$CIP"

LOGGER "Fixing Nameserver"
echo "nameserver $NET_DNS" > /etc/resolv.conf

if [ "$NET_ENABLED" = False ]; then
	rmmod /lib/modules/4.9.170/kernel/drivers/net/wireless/rtl8821cs/8821cs.ko
	exit
fi

insmod /lib/modules/4.9.170/kernel/drivers/net/wireless/rtl8821cs/8821cs.ko
sleep 2

LOGGER "Setting up Wi-Fi Interface"
rfkill unblock all
ip link set "$NET_INTERFACE" up
iw dev "$NET_INTERFACE" set power_save off

LOGGER "Configuring WPA Supplicant"
wpa_supplicant -dd -B -i"$NET_INTERFACE" -c /etc/wpa_supplicant.conf -D nl80211

echo "Obtaining IP" | tr -d '\n' > "$CIP"

if [ "$NET_TYPE" = "DHCP" ]; then
	LOGGER "Clearing DHCP leases"
	rm -rf "/var/db/dhcpcd/*"
	LOGGER "Configuring Network using DHCP (dhcpcd)"
	dhcpcd -n
	dhcpcd "$NET_INTERFACE" &
else
	LOGGER "Configuring Network using Static"
	ip addr add "$NET_ADDRESS"/"$NET_SUBNET" dev "$NET_INTERFACE"
	ip link set dev "$NET_INTERFACE" up
	ip route add default via "$NET_GATEWAY"
fi

LOGGER "Saving Current IP Address"
OIP=0
while [ "$(cat "$CIP")" = "Obtaining IP" ] || [ "$(cat "$CIP")" = "" ]; do
	OIP=$((OIP + 1))
	ip -4 a show dev "$NET_INTERFACE" | sed -nE 's/.*inet ([0-9.]+)\/.*/\1/p' | tr -d '\n' > "$CIP"
	sleep 1
	if [ $OIP -eq 15 ]; then
		echo "Could Not Connect" | tr -d '\n' > "$CIP"
	fi
done

if [ "$(cat "$CIP")" = "Could Not Connect" ]; then
	exit
fi

LOGGER "Running Web Service Script"
/opt/muos/script/web/service.sh

