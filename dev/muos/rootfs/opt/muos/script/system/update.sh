#!/bin/sh
# shellcheck disable=1090,2002

MUOSBOOT_LOG=$1

TWEAK=/opt/muos/config/tweak.txt
. $TWEAK

CDIR=$(pwd)

CURRENT_DATE=$(date +"%Y_%m_%d__%H_%M_%S")

ARCHIVE_DIR=/mnt/mmc/
UPDATE_SCRIPT=/mnt/mmc/update.sh

LOGGER() {
	_TITLE=$1
	_MESSAGE=$2
	_FORM=$(cat <<EOF
$_TITLE

$_MESSAGE
EOF
	)
	/opt/muos/extra/muxstart "$_FORM" && sleep 0.5
	echo "=== ${CURRENT_DATE} === $_MESSAGE" >> "$MUOSBOOT_LOG"
}

cd / || exit

for ZIP in "$ARCHIVE_DIR"update.*.zip; do
	if [ -e "$ZIP" ]; then
		LOGGER "UPDATING SYSTEM" "Extracting $ZIP"
		unzip -o "$ZIP" -d "$ARCHIVE_DIR" &

		while true; do
			IS_WORKING=$(pgrep unzip)
			RANDOM_LINE=$(awk 'BEGIN{srand();} {if (rand() < 1/NR) selected=$0} END{print selected}' /opt/muos/config/messages.txt)

			LOGGER "UPDATING SYSTEM" "$RANDOM_LINE"

			if [ "$IS_WORKING" = "" ]; then
				break
			fi

			sleep 5
		done

		if [ -s "$UPDATE_SCRIPT" ]; then
			LOGGER "UPDATING SYSTEM" "Running Update Script"
			. "$UPDATE_SCRIPT"
			rm "$UPDATE_SCRIPT"
		fi

		LOGGER "UPDATING SYSTEM" "Cleaning Up $ZIP"
		rm "$ZIP"
	fi
done

cd "$CDIR" || exit

LOGGER "UPDATING SYSTEM" "Syncing Partitions"
sync

