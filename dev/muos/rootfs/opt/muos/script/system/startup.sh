#!/bin/sh
# shellcheck disable=1090,2002

CHARGER_ONLINE=$(cat /sys/class/power_supply/axp2202-usb/online)
if [ "$CHARGER_ONLINE" -eq 1 ]; then
	echo powersave > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
	/opt/muos/extra/muxcharge
fi

echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor

mount -t vfat -o rw,utf8,noatime,nofail /dev/mmcblk0p2 /mnt/boot
mount -t exfat -o rw,utf8,noatime,nofail /dev/mmcblk0p7 /mnt/mmc

TWEAK=/opt/muos/config/tweak.txt
. $TWEAK

if [ "$NIGHT" = true ]; then
	echo 255 > /sys/class/disp/disp/attr/color_temperature
else
	echo 0 > /sys/class/disp/disp/attr/color_temperature
fi

CURRENT_DATE=$(date +"%Y_%m_%d__%H_%M_%S")
if [ "$FACTORYRESET" = true ]; then
	MUOSBOOT_LOG="/tmp/muosboot__${CURRENT_DATE}.log"
else
	MUOSBOOT_LOG="/mnt/mmc/MUOS/log/boot/muosboot__${CURRENT_DATE}.log"
fi

LOGGER() {
if [ "$VERBOSE" = true ]; then
	_TITLE=$1
	_MESSAGE=$2
	_FORM=$(cat <<EOF
$_TITLE

$_MESSAGE
EOF
	)
	/opt/muos/extra/muxstart "$_FORM" && sleep 0.5
	echo "=== ${CURRENT_DATE} === $_MESSAGE" >> "$MUOSBOOT_LOG"
fi
}

LOGGER "BOOTING" "Starting..."

/opt/muos/script/system/hdmi.sh &

echo noop > /sys/devices/platform/soc/sdc0/mmc_host/mmc0/mmc0:59b4/block/mmcblk0/queue/scheduler
echo on > /sys/devices/platform/soc/sdc0/mmc_host/mmc0/power/control

echo 0xF > /sys/devices/system/cpu/autoplug/plug_mask

LOGGER "BOOTING" "Starting Storage Watchdog"
/opt/muos/script/mount/sdcard.sh
/opt/muos/script/mount/usb.sh

LOGGER "BOOTING" "Restoring Volume"
if [ "$VOLUME_LOW" = true ]; then
	cp -f /opt/muos/config/volume_low.txt /opt/muos/config/volume.txt
fi
/opt/muos/script/system/volume.sh restore &

if [ "$FACTORYRESET" = true ]; then
	date 010100002020
	hwclock -w

	/opt/muos/extra/muxdevice
	/opt/muos/extra/muxtimezone
	while [ -e "/opt/muos/flag/ClockSetup" ]; do
		/opt/muos/extra/muxrtc
		if [ -e "/opt/muos/flag/ClockSetup" ]; then
			/opt/muos/extra/muxtimezone
		fi
	done

	/opt/muos/bin/muaudio &
	/opt/muos/bin/mp3play "/opt/muos/factory.mp3" &

	LOGGER "FACTORY RESET" "Initialising Factory Reset Script"
	/opt/muos/script/system/reset.sh "$MUOSBOOT_LOG"

	awk -F "=" '/FACTORYRESET/ {sub(/true/, "false", $2)} 1' OFS="=" $TWEAK > /mnt/boot/temp_opt
	mv /mnt/boot/temp_opt $TWEAK

	awk -F "=" '/VERBOSE/ {sub(/true/, "false", $2)} 1' OFS="=" $TWEAK > /mnt/boot/temp_opt
	mv /mnt/boot/temp_opt $TWEAK

	hwclock -s
else
	hwclock -s
fi

pkill "mp3play"

if [ -s /mnt/mmc/update.zip ]; then
	LOGGER "UPDATING SYSTEM" "Update Found..."
	/opt/muos/script/system/update.sh "$MUOSBOOT_LOG"
fi

NETCON=/opt/muos/config/network.txt
if [ -e $NETCON ]; then
	. $NETCON
	if [ "$NET_ENABLED" = True ]; then
		LOGGER "BOOTING" "Starting Network Services"
		/opt/muos/script/system/network.sh "$MUOSBOOT_LOG" &
	fi
fi

LOGGER "BOOTING" "Starting muX Services"
/opt/muos/script/system/watchdog.sh &

LOGGER "BOOTING" "Setting Command Aliases"
. /opt/muos/script/system/alias.sh &

LOGGER "BOOTING" "Cleaning Dotfiles"
/opt/muos/script/system/dotclean.sh &

LOGGER "BOOTING" "Exporting Diagnostic Messages"
dmesg > "/mnt/mmc/MUOS/log/dmesg__${CURRENT_DATE}.log" &

if [ "$VERBOSE" = true ]; then
	cp "$MUOSBOOT_LOG" /mnt/mmc/MUOS/log/boot/.
fi

/opt/muos/script/mux/launch.sh &

