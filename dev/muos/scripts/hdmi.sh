#!/system/bin/sh

LAST_STATE=-1
STATE_FILE="/sys/class/switch/hdmi/state"
MIRROR_FILE="/sys/devices/framebuffer.4/graphics/fb0/mirror_to_hdmi"

while true; do
	CURRENT_STATE=$(cat "$STATE_FILE")
	if [ "$LAST_STATE" != "$CURRENT_STATE" ]; then
		echo "$CURRENT_STATE" > "$MIRROR_FILE"
		LAST_STATE="$CURRENT_STATE"
	fi
	sleep 5
done &
