#!/system/bin/sh

# Grab the variables from the following file
. /misc/options.txt

MOTO=/sys/class/power_supply/battery/moto

process_rumble() {
    while IFS= read -r line; do
        case "$line" in
            ""|"#"* ) continue ;;
            * )
                STRENGTH=$(echo "$line" | busybox awk '{print $1}')
                LENGTH=$(echo "$line" | busybox awk '{print $2}')
                PAUSE=$(echo "$line" | busybox awk '{print $3}')

                echo "ST=$STRENGTH LN=$LENGTH PA=$PAUSE"

                echo "$STRENGTH" > "$MOTO" & busybox sleep "$LENGTH"
                echo 0 > "$MOTO" & busybox sleep "$PAUSE"
                ;;
        esac
    done < "$THM"/rumble.txt
}

# Start the boot animation process
if [ "$FACTORYRESET" = false ] && [ "$RETRORESET" = false ]; then
	if [ -d /misc/theme ]; then
		THM=/misc/theme/"$THEME"
		if [ -f "$THM"/config.txt ]; then
			echo 1 > /tmp/animwait

			# Grab the configuration of the theme specified
			. "$THM"/config.txt

			echo "$BRIGHTNESS" > /sys/class/backlight/backlight.2/brightness

			if [ "$STYLE" = acr ] || [ "$STYLE" = ar ] || [ "$STYLE" = cr ] || [ "$STYLE" = r ]; then
				if [ "$RUMBLE" = true ] && [ -f "$THM"/rumble.txt ]; then
					process_rumble &
				fi
			fi

			if [ "$STYLE" = acr ] || [ "$STYLE" = ac ] || [ "$STYLE" = cr ] || [ "$STYLE" = c ]; then
				if [ "$SOUND" = true ]; then
					MP3C=$(busybox find "$THM/chime" -type f -name "*.mp3" | busybox wc -l)
					CHIME="$THM/chime/$((RANDOM % MP3C)).mp3"
					mp3play "$CHIME" &
				fi
			fi

			if [ "$STYLE" = acr ] || [ "$STYLE" = ac ] || [ "$STYLE" = ar ] || [ "$STYLE" = a ]; then
				busybox seq 1 "$LOOP" | while read -r _; do
					for F in $(busybox seq 1 "$END_FRAME"); do
						fbim "$THM/image/$F".png &
						busybox sleep "$TIMING"
					done
				done
			fi

			busybox sleep "$SLEEP"
			echo 0 > /tmp/animwait

			busybox killall mp3play
		fi
	fi
fi
