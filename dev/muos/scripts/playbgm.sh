#!/system/bin/sh

mp3_dir="/mnt/mmc/INFO/music"
last_played_file="$mp3_dir/last_played.txt"

while true; do
	cd "$mp3_dir" || exit 1
	touch "$last_played_file"

	mp3_files=$(busybox find . -maxdepth 1 -type f -name "*.mp3" -a ! -name "$(busybox cat "$last_played_file")")

	if [ -n "$mp3_files" ]; then
		num_lines=$(busybox echo "$mp3_files" | busybox wc -l)
		random_line=$(busybox awk -v min=1 -v max="$num_lines" 'BEGIN{srand(); print int(min+rand()*(max-min+1))}')

		selected_mp3=$(busybox echo "$mp3_files" | busybox sed -n "${random_line}p")

		busybox echo "$(busybox basename "$selected_mp3")" > "$last_played_file"
		/system/bin/mp3play "$selected_mp3"
	fi

	sleep 10
done
