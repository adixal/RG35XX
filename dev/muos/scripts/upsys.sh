#!/bin/bash

cd ROMS
7z a -t7z -m0=lzma -mx=9 -mfb=64 -md=64m -ms=on system.7z BIOS CORE IMGS LIST LOGS MUOS ROMS SAVE autorun.inf muoslogo.ico
mv system.7z ../SYSTEM/data/system.7z
cd MUOS
7z a -t7z -m0=lzma -mx=9 -mfb=64 -md=64m -ms=on retroarch.7z .retroarch retroarch muos.cfg
mv retroarch.7z ../../SYSTEM/data/retroarch.7z
cd ../../
