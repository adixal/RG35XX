#!/system/bin/sh

# Grab the variables from the following file
. /misc/options.txt

DTB=/system/data/dtbs/"$BATTERY"-"$FILTER".dtb

if [ -f "$DTB" ]; then
    cp "$DTB" /misc/kernel.dtb
fi
