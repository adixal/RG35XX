#!/bin/sh

if [ -z "$1" ]; then
    echo "Usage: $0 <filename>"
    exit 1
fi

filename="$1"

if [ ! -f "$filename" ]; then
    echo "File not found: $filename"
    exit 1
fi

invalid_chars='*?.,;:/\|+=<>[]"'
placeholder='\^'

busybox sed -i "s/=/$placeholder/g" "$filename"
busybox tr  -d "[$invalid_chars]" < "$filename" > "${filename}_temp"
busybox mv     "${filename}_temp"   "$filename"
busybox sed -i "s/$placeholder/=/1" "$filename"
