#!/bin/bash

if [ "$#" -ne 3 ]; then
    echo "Usage: $0 input_url output_filename start_time_in_seconds"
    exit 1
fi

input_url="$1"
output_filename="$2"
start_time="$3"
duration=10

if [ -e "$output_filename.gif" ]; then
    rm "$output_filename.gif"
fi

if [[ "$input_url" == http://* || "$input_url" == https://* ]]; then
    curl -o "output.mp4" "$input_url"
else
    yt-dlp --quiet --progress -f 'best[height=360]/mp4' -o "output.mp4" "$input_url"
fi

start_time=$(printf "%02d:%02d:%02d" $((start_time / 3600)) $((start_time % 3600 / 60)) $((start_time % 60)))

ffmpeg -v warning -y -ss "$start_time" -t "$duration" -i output.mp4 -filter_complex "fps=15,scale=-1:200:flags=lanczos,split[s0][s1];[s0]palettegen=max_colors=96:stats_mode=diff[p];[s1][p]paletteuse=dither=bayer" output.gif
gifsicle --no-conserve-memory -O3 -j"$(nproc)" -i output.gif -o "$output_filename.gif"

ls -lh "$output_filename.gif"

rm output.*
