#!/system/bin/sh

busybox sleep 10

batt_seq() {
	busybox seq 1 5 | while read -r _; do
		busybox sleep 0.5
		echo 1 > /sys/class/backlight/backlight.2/bl_power
		busybox sleep 0.2
		echo 0 > /sys/class/backlight/backlight.2/bl_power
	done
}

while true; do
	CAPACITY=$(cat /sys/class/power_supply/battery/capacity)
	if [ $CAPACITY -le $LOWBATTERY ]; then batt_seq; fi
	busybox sleep 300
done &

