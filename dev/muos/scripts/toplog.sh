#!/system/bin/sh

log_file="/mnt/mmc/process.log"
iterations=60
interval=1

echo "Timestamp | CPU Usage (%) | Process CPU Usage (%)" > "$log_file"

i=0
while [ "$i" -lt "$iterations" ]; do
    i=$((i + 1))
    timestamp=$(busybox date +"%Y-%m-%d %H:%M:%S")
    cpu_usage=$(busybox top -b -n 1 | busybox awk '/%Cpu/{print $2}')
    process_cpu_usage=$(busybox top -b -n 1 | busybox awk '/PID/{getline; print $9}')

    echo "$timestamp | $cpu_usage | $process_cpu_usage" >> "$log_file"
    sleep "$interval"
done &
