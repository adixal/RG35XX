#!/bin/sh

TZ_PATH="/usr/share/zoneinfo/posix"
OT_HEAD="timezone.h"

echo "const char* timezone[] = {" > "$OT_HEAD"

find "$TZ_PATH/" -type f | sed -e "s|$TZ_PATH||" | sed -e 's|^/||' | sort | while IFS= read -r TZ; do
    if [ "$TZ" != "." ] && [ "$TZ" != ".." ]; then
        echo "    \"$TZ\"," >> "$OT_HEAD"
    fi
done

echo "};" >> "$OT_HEAD"

