#!/system/bin/sh
# shellcheck disable=SC2028

# Grab the variables from the following file
. /misc/options.txt

# Set /misc to read write
mount -o remount,rw /misc

START_LOG_TIME=$(date -u +%s)
AC_CONNECT="/sys/class/power_supply/battery/charger_online"

if [ "$BOOTLOG" = true ]; then
	timestamp=$(date -u +"%Y-%m-%d %H:%M:%S")
	{
		echo "| Mode | Status |"
		echo "| ---- | ------ |"
		echo "| BOOT | \`$timestamp\` |"
		echo "| BOOT | Starting muOS |"
	} >> /misc/boot.log
fi

PIPE=/tmp/muxpipe
[ "$BOOTLOG" = true ] && echo "BOOTING\n\nStarting muOS" > "$PIPE"

# The MUOS root directory
MUOS=/mnt/mmc/MUOS
CDIR=$(pwd)

# Make sure the bits_per_pixel is set to 16
[ "$BOOTLOG" = true ] && echo "| BOOT | Setting \`bits_per_pixel\` to \`16\` |" >> /misc/boot.log
echo 16 > /sys/devices/framebuffer.4/graphics/fb0/bits_per_pixel

# Disable CPU hotplug
[ "$BOOTLOG" = true ] && echo "| BOOT | Disabling CPU hotplug |" >> /misc/boot.log
echo 0xF > /sys/devices/system/cpu/autoplug/plug_mask

# Switch to the performance governor for a boot boost
[ "$BOOTLOG" = true ] && echo "| BOOT | Switching governor to \`performance\` mode |" >> /misc/boot.log
echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor

# Switch USB into device mode and enable ADB if needed (for developers)
if [ "$ADB" = true ]; then
	[ "$BOOTLOG" = true ] && echo "BOOTING\n\nEnabling ADB" > "$PIPE" && sleep 0.75
	[ "$BOOTLOG" = true ] && echo "| BOOT | Enabling ADB |" >> /misc/boot.log
	/usbdbg.sh device &
fi

# Enable HDMI output if enabled
if [ "$HDMI" = true ]; then
	[ "$BOOTLOG" = true ] && echo "BOOTING\n\nEnabling HDMI" > "$PIPE" && sleep 0.75
	[ "$BOOTLOG" = true ] && echo "| BOOT | Enabling HDMI |" >> /misc/boot.log
	/system/data/hdmi.sh
fi

# Switch the backlight on because of reasons
[ "$BOOTLOG" = true ] && echo "| BOOT | Switching backlight power on |" >> /misc/boot.log
echo 0 > /sys/class/backlight/backlight.2/bl_power

# Configure ALSA symlinks since the system placed them in weird locations
mkdir /usr/share && mkdir /usr/share/alsa
[ "$BOOTLOG" = true ] && echo "BOOTING\n\nFixing ALSA Device Mapping" > "$PIPE" && sleep 0.75
[ "$BOOTLOG" = true ] && echo "| BOOT | Fixing AlSA Device Mapping |" >> /misc/boot.log
for a in alsa.conf cards pcm; do
	[ "$BOOTLOG" = true ] && echo "| ALSA | Linking \`$a\` |" >> /misc/boot.log
	ln -s /system/usr/share/alsa/"$a" /usr/share/alsa/"$a" &
done

# Function to use for updating and regular booting
mount_muos() {
	[ "$BOOTLOG" = true ] && echo "| LOOP | Setting muOS loop device variables |" >> /misc/boot.log
	MUOS_IMAGE=/system/data/muos.img
	MUOS_MOUNTPOINT=/cfw
	LOOPDEVICE=/dev/block/loop7

	# Mount the uClibc-based muOS filesystem
	[ "$BOOTLOG" = true ] && echo "| LOOP | Creating mountpoint directory |" >> /misc/boot.log
	mkdir $MUOS_MOUNTPOINT
	[ "$BOOTLOG" = true ] && echo "| LOOP | Setting up loopback device |" >> /misc/boot.log
	busybox losetup $LOOPDEVICE $MUOS_IMAGE
	[ "$BOOTLOG" = true ] && echo "| LOOP | Mounting loopback device |" >> /misc/boot.log
	mount -r -w -o loop -t ext4 $LOOPDEVICE $MUOS_MOUNTPOINT

	# Create mount points
	for m in mmc sdcard; do
		[ "$BOOTLOG" = true ] && echo "| LOOP | Mountpoint \`$m\` created |" >> /misc/boot.log
		mkdir $MUOS_MOUNTPOINT/mnt/"$m"
	done

	# Mount system devices and external SD cards
	for d in dev dev/pts proc sys run mnt/mmc mnt/sdcard; do
		[ "$BOOTLOG" = true ] && echo "| LOOP | Binding \`$d\` to loop |" >> /misc/boot.log
		mount -o bind /"$d" $MUOS_MOUNTPOINT/"$d"
	done
}

# First time booting or just fucked up? Time for a factory reset!
if [ "$FACTORYRESET" = true ]; then
	[ "$BOOTLOG" = true ] && echo "| INIT | Starting factory reset |" >> /misc/boot.log
	echo "FACTORY RESET\n\nStarting Factory Reset" > "$PIPE" && sleep 0.5

	[ "$BOOTLOG" = true ] && echo "| INIT | Waiting for AC connection |" >> /misc/boot.log
	echo "FACTORY RESET\n\nPlug in charger to continue!" > "$PIPE" && sleep 0.5
	while [ "$(cat "$AC_CONNECT")" -ne 1 ]; do
		sleep 1
	done

	[ "$BOOTLOG" = true ] && echo "| INIT | Remounting \`/system\` directory |" >> /misc/boot.log
	mount -o remount,rw /system
	[ "$BOOTLOG" = true ] && echo "| INIT | Unmounting \`/mnt/mmc\` directory |" >> /misc/boot.log
	umount /mnt/mmc
	echo "FACTORY RESET\n\nExpanding SD1 ROM Partition" > "$PIPE" && sleep 0.5
	[ "$BOOTLOG" = true ] && echo "| INIT | Recreating SD1 ROM partition |" >> /misc/boot.log
	busybox printf "fix\n" | parted ---pretend-input-tty /dev/block/mmcblk0 print
	[ "$BOOTLOG" = true ] && echo "| INIT | Expanding SD1 ROM partition |" >> /misc/boot.log
	parted ---pretend-input-tty /dev/block/mmcblk0 resizepart 4 100%
	echo "FACTORY RESET\n\nFormatting SD1 ROM Partition" > "$PIPE" && sleep 0.5
	[ "$BOOTLOG" = true ] && echo "| INIT | Formatting SD1 ROM partition |" >> /misc/boot.log
	mkfs.fat /dev/block/mmcblk0p4
	[ "$BOOTLOG" = true ] && echo "| INIT | Labelling SD1 ROM partition |" >> /misc/boot.log
	dosfslabel /dev/block/mmcblk0p4 ROMS
	echo "FACTORY RESET\n\nSetting SD1 ROM Partition Flags" > "$PIPE" && sleep 0.5
	[ "$BOOTLOG" = true ] && echo "| INIT | Setting SD1 ROM partition flags |" >> /misc/boot.log
	parted ---pretend-input-tty /dev/block/mmcblk0 set 4 boot off
	parted ---pretend-input-tty /dev/block/mmcblk0 set 4 hidden off
	[ "$BOOTLOG" = true ] && echo "| INIT | Mounting \`/mnt/mmc\` partition |" >> /misc/boot.log
	echo "FACTORY RESET\n\nRestoring SD1 ROM Filesystem" > "$PIPE" && sleep 0.5
	mount -t vfat /dev/block/mmcblk0p4 /mnt/mmc
	[ "$BOOTLOG" = true ] && echo "| INIT | Extracting \`system.7z\` archive to ROM partition |" >> /misc/boot.log
	cd /mnt/mmc && /system/data/extract.sh /system/data/system.7z "FACTORY RESET\n\nRestoring SD1 ROM Filesystem" /tmp/muxpipe && cd "$CDIR" || exit
	echo "FACTORY RESET\n\nSyncing Partitions" > "$PIPE" && sleep 0.5
	[ "$BOOTLOG" = true ] && echo "| INIT | Changing \`factoryreset\` to \`false\` in \`options.txt\` |" >> /misc/boot.log
	gawk -F "=" '/FACTORYRESET/ {sub(/true/, "false", $2)} 1' OFS="=" /misc/options.txt > /misc/temp
	mv /misc/temp /misc/options.txt
	[ "$BOOTLOG" = true ] && echo "| INIT | Synchronising filesystem |" >> /misc/boot.log
	sync
	if [ "$SECRET" = mustard ]; then
		[ "$BOOTLOG" = true ] && echo "| INIT | Secret mustard mode found |" >> /misc/boot.log
		echo "MUSTARD MODE\n\nHey, you found the secret!" > "$PIPE" && sleep 0.75
		echo "MUSTARD MODE\n\nYou're awesome!" > "$PIPE" && sleep 0.75
		echo "MUSTARD MODE\n\nThank you so much for using muOS" > "$PIPE" && sleep 0.75
		echo "MUSTARD MODE\n\nIt's been almost 12 months\ndeveloping muOS" > "$PIPE" && sleep 0.75
		echo "MUSTARD MODE\n\nLet Adixal know you've found this\nalong with a screenshot..." > "$PIPE" && sleep 0.75
		echo "MUSTARD MODE\n\nWell I hope you enjoy muOS\nas much as I have enjoyed\ndeveloping it!" > "$PIPE" && sleep 1
	fi
	[ "$BOOTLOG" = true ] && echo "| INIT | Continue to muOS launcher |" >> /misc/boot.log
fi

# Time to reset all of RetroArch settings and configs
if [ "$RETRORESET" = true ]; then
	[ "$BOOTLOG" = true ] && echo "| RRES | Starting retroarch reset |" >> /misc/boot.log
	echo "RETROARCH RESET\n\nStarting Reset" > "$PIPE" && sleep 0.5
	[ "$BOOTLOG" = true ] && echo "| RRES | Remounting \`/system\` directory |" >> /misc/boot.log
	mount -o remount,rw /system
	[ "$BOOTLOG" = true ] && echo "| RRES | Removing retroarch configs |" >> /misc/boot.log
	echo "RETROARCH RESET\n\nRemoving RetroArch Configs" > "$PIPE" && sleep 0.5
	rm -rf /mnt/mmc/INFO
	rm -rf /mnt/sdcard/INFO
	rm -rf /mnt/mmc/MUOS/.retroarch
	[ "$BOOTLOG" = true ] && echo "| RRES | Extracting \`retroarch.7z\` archive |" >> /misc/boot.log
	echo "RETROARCH RESET\n\nExtracting RetroArch Archive" > "$PIPE" && sleep 0.5
	cd /mnt/mmc && /system/data/extract.sh /system/data/retroarch.7z "RETROARCH RESET\n\nExtracting RetroArch Archive" /tmp/muxpipe && cd "$CDIR" || exit
	[ "$BOOTLOG" = true ] && echo "| RRES | Changing \`retroreset\` to \`false\` in \`options.txt\` |" >> /misc/boot.log
	gawk -F "=" '/RETRORESET/ {sub(/true/, "false", $2)} 1' OFS="=" /misc/options.txt > /misc/temp
	mv /misc/temp /misc/options.txt
	echo "RETROARCH RESET\n\nSyncing partitions" > "$PIPE" && sleep 0.5
	[ "$BOOTLOG" = true ] && echo "| RRES | Synchronising filesystem |" >> /misc/boot.log
	sync
	[ "$BOOTLOG" = true ] && echo "| RRES | Continue to muOS launcher |" >> /misc/boot.log
fi

# Time to update the system! This should update all partitions if required.
if [ -f /misc/update.7z ]; then
	[ "$BOOTLOG" = true ] && echo "| UPDT | Starting system update |" >> /misc/boot.log
	echo "SYSTEM UPDATE\n\nStarting Update" > "$PIPE" && sleep 0.5

	[ "$BOOTLOG" = true ] && echo "| UPDT | Waiting for AC connection |" >> /misc/boot.log
	echo "SYSTEM UPDATE\n\nPlug in charger to continue!" > "$PIPE" && sleep 0.5
	while [ "$(cat "$AC_CONNECT")" -ne 1 ]; do
		sleep 1
	done

	mount_muos
	[ "$BOOTLOG" = true ] && echo "| UPDT | Remounting \`/system\` directory |" >> /misc/boot.log
	mount -o remount,rw /system
	[ "$BOOTLOG" = true ] && echo "| UPDT | Extracting system update |" >> /misc/boot.log
	echo "SYSTEM UPDATE\n\nExtracting Update" > "$PIPE" && sleep 0.5
	cd / && /system/data/extract.sh /misc/update.7z "SYSTEM UPDATE\n\nExtracting Update" /tmp/muxpipe && cd "$CDIR" || exit
	if [ -f /mnt/mmc/update.sh ]; then
		[ "$BOOTLOG" = true ] && echo "| UPDT | Executing update script |" >> /misc/boot.log
		echo "SYSTEM UPDATE\n\nExecuting Update Script" > "$PIPE" && sleep 0.5
		./mnt/mmc/update.sh
	fi
	echo "SYSTEM UPDATE\n\nCleaning Up" > "$PIPE" && sleep 0.5
	[ "$BOOTLOG" = true ] && echo "| UPDT | Removing update archive |" >> /misc/boot.log
	rm /misc/update.7z
	[ "$BOOTLOG" = true ] && echo "| UPDT | Removing update script |" >> /misc/boot.log
	rm /mnt/mmc/update.sh
	[ "$BOOTLOG" = true ] && echo "| UPDT | Synchronising filesystem |" >> /misc/boot.log
	echo "SYSTEM UPDATE\n\nSyncing Partitions" > "$PIPE" && sleep 0.5
	sync
	[ "$BOOTLOG" = true ] && echo "| UPDT | Restarting device |" >> /misc/boot.log
	echo "SYSTEM UPDATE\n\nRestarting Device" > "$PIPE" && sleep 1
	reboot
fi

# Wait a bit for some controllers that need time to initialise
if [ "$GAMEPAD" = true ]; then
	[ "$BOOTLOG" = true ] && echo "| BOOT | Waiting for gamepads to initialise |" >> /misc/boot.log
	[ "$BOOTLOG" = true ] && echo "BOOTING\n\nWaiting for Gamepads" > "$PIPE" && sleep 0.75
	sleep 5
fi

# Disable the IO scheduler (it just ends up slowing down the MicroSD cards)
[ "$BOOTLOG" = true ] && echo "BOOTING\n\nDisabling IO Scheduler" > "$PIPE" && sleep 0.75
[ "$BOOTLOG" = true ] && echo "| BOOT | Disabling \`mmc\` queue scheduler |" >> /misc/boot.log
echo noop > /sys/devices/b0238000.mmc/mmc_host/mmc0/emmc_boot_card/block/mmcblk0/queue/scheduler
[ "$BOOTLOG" = true ] && echo "| BOOT | Disabling \`sdcard\` queue scheduler |" >> /misc/boot.log
echo noop > /sys/devices/b0230000.mmc/mmc_host/mmc1/sd_card/block/mmcblk1/queue/scheduler

# Disable MicroSD card power-saving (helps reduce load stutter)
[ "$BOOTLOG" = true ] && echo "BOOTING\n\nDisabling SD Power Saving" > "$PIPE" && sleep 0.75
[ "$BOOTLOG" = true ] && echo "| BOOT | Disabling \`mmc\` power saving |" >> /misc/boot.log
echo on > /sys/devices/b0238000.mmc/mmc_host/mmc0/power/control
[ "$BOOTLOG" = true ] && echo "| BOOT | Disabling \`sdcard\` power saving |" >> /misc/boot.log
echo on > /sys/devices/b0230000.mmc/mmc_host/mmc1/power/control

# Mount the second MicroSD card
BLK=/dev/block/mmcblk
mkdir /mnt/sdcard
[ "$BOOTLOG" = true ] && echo "| BOOT | Detecting SD2 card |" >> /misc/boot.log
if [ -e "$BLK"1p1 ]; then
	[ "$BOOTLOG" = true ] && echo "| BOOT | SD2 \`mmcblk1p1\` found |" >> /misc/boot.log
	SDCARD_DEVICE="$BLK"1p1
else
	[ "$BOOTLOG" = true ] && echo "| BOOT | SD2 \`mmcblk1\` found |" >> /misc/boot.log
	SDCARD_DEVICE="$BLK"1
fi

# Try FAT32 mounting first
[ "$BOOTLOG" = true ] && echo "| BOOT | Attempting to mount SD2 with \`FAT32\` |" >> /misc/boot.log
F32M=$(mount -t vfat -o rw,utf8,noatime $SDCARD_DEVICE /mnt/sdcard)
if [ ! "$F32M" ]; then
	[ "$BOOTLOG" = true ] && echo "| BOOT | Attempting to mount SD2 with \`EXT4\` |" >> /misc/boot.log
	mount -t ext4 -o rw,utf8,noatime $SDCARD_DEVICE /mnt/sdcard
fi

# Mount the uClibc-based muOS filesystem from the above function
[ "$BOOTLOG" = true ] && echo "BOOTING\n\nMounting muOS Filesystem" > "$PIPE" && sleep 0.75
mount_muos

# Start the watchdog process which looks out for the following:
# muAudio, muBright
[ "$BOOTLOG" = true ] && echo "BOOTING\n\nStarting Internal Services" > "$PIPE" && sleep 0.75
[ "$BOOTLOG" = true ] && echo "| BOOT | Starting \`muwatch\` process |" >> /misc/boot.log
busybox chroot $MUOS_MOUNTPOINT /usr/bin/muwatch &

# Set local variables
[ "$BOOTLOG" = true ] && echo "| BOOT | Setting local variables |" >> /misc/boot.log
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:"$PATH"
export HOME=$MUOS
export SHELL=/bin/sh
export SDL_NOMOUSE=1

[ "$BOOTLOG" = true ] && echo "| BOOT | Attempting to navigate home |" >> /misc/boot.log
cd $HOME || exit

if [ "$SCREENSHOT" = mmc ] || [ "$SCREENSHOT" = sdcard ]; then
	[ "$BOOTLOG" = true ] && echo "BOOTING\n\nEnabling Screenshot on '$SCREENSHOT'" > "$PIPE" && sleep 0.75
	[ "$BOOTLOG" = true ] && echo "| BOOT | Enabling \`muscreen\` process at \`$SCREENSHOT\` |" >> /misc/boot.log
	busybox chroot $MUOS_MOUNTPOINT /usr/bin/muscreen "$SCREENSHOT"
fi

# Setup SWAP space
[ "$BOOTLOG" = true ] && echo "BOOTING\n\nMounting Swap Space" > "$PIPE" && sleep 0.75
[ "$BOOTLOG" = true ] && echo "| BOOT | Creating swap file on \`mmcblk0p3\` |" >> /misc/boot.log
busybox mkswap /dev/block/mmcblk0p3
[ "$BOOTLOG" = true ] && echo "| BOOT | Enabling swap file on \`mmcblk0p3\` |" >> /misc/boot.log
busybox swapon /dev/block/mmcblk0p3

# Process logger for debug purposes
if [ "$TOPLOG" = true ]; then
	[ "$BOOTLOG" = true ] && echo "BOOTING\n\nEnabling TOP Logger" > "$PIPE" && sleep 0.75
	[ "$BOOTLOG" = true ] && echo "| BOOT | Logging top process information |" >> /misc/boot.log
	/system/data/toplog.sh &
fi

# Symlink bin for muosutil
[ "$BOOTLOG" = true ] && echo "| BOOT | Force removing \`/bin\` directory |" >> /misc/boot.log
rm -rf /bin
[ "$BOOTLOG" = true ] && echo "| BOOT | Symlinking \`/system/bin\` back to \`/bin\` |" >> /misc/boot.log
ln -s /system/bin /bin

# Audio Device Debugging
if [ "$AUDIODEBUG" = true ]; then
	[ "$BOOTLOG" = true ] && echo "BOOTING\n\nExporting Audio Settings" > "$PIPE" && sleep 0.75
	[ "$BOOTLOG" = true ] && echo "| BOOT | Exporting audio device information |" >> /misc/boot.log
	busybox chroot $MUOS_MOUNTPOINT /usr/bin/amixer > /mnt/mmc/audiodev.txt
fi

# Change /system to read/write mode
if [ "$SYSTEMRW" = true ]; then
	[ "$BOOTLOG" = true ] && echo "BOOTING\n\nMounting '/system' in RW Mode" > "$PIPE" && sleep 0.75
	[ "$BOOTLOG" = true ] && echo "| BOOT | Remounting \`/system\` in read write mode |" >> /misc/boot.log
	mount -o remount,rw /system &
fi

# Start the low battery indicator
[ "$BOOTLOG" = true ] && echo "BOOTING\n\nStarting Low Battery Script" > "$PIPE" && sleep 0.75
[ "$BOOTLOG" = true ] && echo "| BOOT | Starting low battery indicator script |" >> /misc/boot.log
/system/data/lowbatt.sh &

# Start the auto brightness module
[ "$BOOTLOG" = true ] && echo "BOOTING\n\nStarting Auto Brightness Script" > "$PIPE" && sleep 0.75
[ "$BOOTLOG" = true ] && echo "| BOOT | Starting auto brightness script |" >> /misc/boot.log
/system/data/bright.sh &

# Start muSleep with settings from config
[ "$BOOTLOG" = true ] && echo "BOOTING\n\nStarting Idle Program" > "$PIPE" && sleep 0.75
[ "$BOOTLOG" = true ] && echo "| BOOT | Starting \`musleep\` with \`$BLANK\` and \`$SHUTDOWN\` |" >> /misc/boot.log
busybox chroot $MUOS_MOUNTPOINT /usr/bin/musleep "$BLANK" "$SHUTDOWN" &

# Run the fixname.sh script to remove invalid characters from labels
[ "$BOOTLOG" = true ] && echo "BOOTING\n\nRemoving Invalid Names" > "$PIPE" && sleep 0.75
[ "$BOOTLOG" = true ] && echo "| BOOT | Removing invalid characters in \`name.txt\` file on \`mmc\` |" >> /misc/boot.log
/system/data/fixname.sh /mnt/mmc/INFO/name.txt
[ "$BOOTLOG" = true ] && echo "| BOOT | Removing invalid characters in \`name.txt\` file on \`sdcard\` |" >> /misc/boot.log
/system/data/fixname.sh /mnt/sdcard/INFO/name.txt

# Let us see if the SD2 exists and if it does check for the INFO directory
if grep -q "mmcblk1" /proc/partitions && [ ! -d "/mnt/sdcard/INFO" ]; then
	[ "$BOOTLOG" = true ] && echo "BOOTING\n\nCreating 'INFO' on SD2" > "$PIPE" && sleep 0.75
	[ "$BOOTLOG" = true ] && echo "| BOOT | Creating \`INFO\` directory on SD2 |" >> /misc/boot.log
	mkdir -p "/mnt/sdcard/INFO"
fi

# Okay if the INFO directory exists then copy over the files if they don't
if [ -d "/mnt/sdcard/INFO" ]; then
	for file in "name.txt" "core.txt"; do
		if [ ! -e "/mnt/sdcard/INFO/$file" ]; then
			[ "$BOOTLOG" = true ] && echo "BOOTING\n\nCopying '$file' to 'INFO' on SD2" > "$PIPE" && sleep 0.75
			[ "$BOOTLOG" = true ] && echo "| BOOT | Copying \`$file\` to SD2 \`INFO\` directory |" >> /misc/boot.log
			cp "/mnt/mmc/INFO/$file" "/mnt/sdcard/INFO"
		fi
	done
	mkdir "/mnt/sdcard/INFO/content"
	mkdir "/mnt/sdcard/SAVE"
	mkdir "/mnt/sdcard/SAVE/FILE"
	mkdir "/mnt/sdcard/SAVE/STATE"
fi

# Let the kernel cache all of the things
[ "$BOOTLOG" = true ] && echo "| BOOT | Caching \`INFO\` on \`mmc\` |" >> /misc/boot.log
busybox find /mnt/mmc/INFO &
[ "$BOOTLOG" = true ] && echo "| BOOT | Caching \`INFO\` on \`sdcard\` |" >> /misc/boot.log
busybox find /mnt/sdcard/INFO &

# Create a temporary file for the launch action
[ "$BOOTLOG" = true ] && echo "| BOOT | Creating temp files for variable storage |" >> /misc/boot.log
ACT_GO=/tmp/act_go
SYS_GO=/tmp/sys_go
ROM_GO=/tmp/rom_go

ROM_LAST=/tmp/rom_last

# Set the starting action for the device
[ "$BOOTLOG" = true ] && echo "BOOTING\n\nThank you for using muOS!" > "$PIPE" && sleep 1.25
[ "$BOOTLOG" = true ] && echo "| BOOT | Starting on \`$STARTUP\` mode |" >> /misc/boot.log
echo "$STARTUP" > $ACT_GO
echo "quit" > "$PIPE"

CARD=""
SYSTEM=""
LAST_PLAY="/mnt/mmc/MUOS/.lastplay"

# Debug things!
if [ "$HALTBOOT" = true ]; then
	[ "$BOOTLOG" = true ] && echo "BOOTING\n\nEntering in Development Mode" > "$PIPE" && sleep 0.75
	[ "$BOOTLOG" = true ] && echo "| BOOT | Development mode initiated |" >> /misc/boot.log
	echo 1 > /sys/class/backlight/backlight.2/bl_power
	while true; do sleep 300; done
fi

END_LOG_TIME=$(date -u +%s)
DIFF_LOG_TIME=$((END_LOG_TIME - START_LOG_TIME))

if [ "$BOOTLOG" = true ]; then
	timestamp=$(date -u +"%Y-%m-%d %H:%M:%S")
	{
		echo "| BOOT | \`$timestamp\` \`$DIFF_LOG_TIME SECONDS\` |"
		echo "---"
	} >> /misc/boot.log

	# Disable the boot log since it's no longer needed
	[ "$BOOTLOG" = true ] && echo "| BOOT | Stopping boot logger |" >> /misc/boot.log
	gawk -F "=" '/BOOTLOG/ {sub(/true/, "false", $2)} 1' OFS="=" /misc/options.txt > /misc/temp
	mv /misc/temp /misc/options.txt
	sync
fi

busybox pkill -x muxstart
BGM_PID=""

while true; do
	. /misc/options.txt

	# Background Music
	if [ "$MSOUND" = bgm ]; then
		if ! busybox pgrep -x "mp3play" > /dev/null; then
			/system/data/playbgm.sh &
			BGM_PID=$!
		fi
	else
		if [ -n "$BGM_PID" ]; then
			busybox kill "$BGM_PID"
			BGM_PID=""
		fi
		busybox pkill -x mp3play
	fi

	# System Loader
	if [ -s "$SYS_GO" ] && [ ! -s "$ROM_GO" ]; then
		echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor

		CARD=$(busybox cat "$SYS_GO" | busybox sed -n '1p')
		SYSTEM=$(busybox cat "$SYS_GO" | busybox sed -n '2p')

		echo romlist > $ACT_GO
	fi

	# Content Loader
	if [ -s "$ROM_GO" ]; then
		cat $ROM_GO > $ROM_LAST

		CORE=$(busybox cat "$ROM_GO" | busybox sed -n '1p')
		ROM=$(busybox cat "$ROM_GO" | busybox sed -n '2p')
		CARD=$(busybox cat "$ROM_GO" | busybox sed -n '3p')
		SYSTEM=$(busybox cat "$ROM_GO" | busybox sed -n '4p')
		NAME=$(busybox cat "$ROM_GO" | busybox sed -n '5p')

		rm $ROM_GO

		busybox kill "$BGM_PID"
		busybox pkill -x mp3play

		if [ "$CORE" = external ]; then
			busybox chroot "$MUOS_MOUNTPOINT" "$ROM"
		else
			if [ ! -e "/mnt/$CARD/INFO/content/$SYSTEM/config/$NAME.cfg" ]; then
				cp "/mnt/mmc/MUOS/.retroarch/retroarch-$CARD.cfg" "/mnt/$CARD/INFO/content/$SYSTEM/config/$NAME.cfg"
			fi
			busybox chroot "$MUOS_MOUNTPOINT" "$HOME/retroarch" -c "/mnt/$CARD/INFO/content/$SYSTEM/config/$NAME.cfg" -L "/mnt/mmc/CORE/$CORE" "$ROM"
		fi

		cat $ROM_LAST > $LAST_PLAY
		[ "$(cat $ACT_GO)" = last ] && echo launcher > $ACT_GO

		continue
	fi

	if [ "$(cat $ACT_GO)" = last ] && [ -s "$LAST_PLAY" ]; then
		LPC=$(busybox cat "$LAST_PLAY")

		for i in 1 2 3 4 5; do
			LINE=$(echo "$LPC" | busybox sed -n "${i}p")
			[ -n "$LINE" ] && echo "$LINE" >> "$ROM_GO"
		done

		for i in 3 4; do
			LINE=$(echo "$LPC" | busybox sed -n "${i}p")
			[ -n "$LINE" ] && echo "$LINE" >> "$SYS_GO"
		done

		rm "$LAST_PLAY"

		continue
	fi

	echo powersave > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor

	if [ -s "/tmp/mux_suppress" ]; then
		MSG_SUPPRESS=$(busybox cat "/tmp/mux_suppress")
		rm "/tmp/mux_suppress"
	else
		MSG_SUPPRESS=0
	fi

	if [ "$(cat $ACT_GO)" = romlist ] || [ "$(cat $ACT_GO)" = favourite ] || [ "$(cat $ACT_GO)" = history ]; then
		if [ -s "/tmp/mux_lastindex_rom" ]; then
			LAST_INDEX_ROM=$(busybox cat "/tmp/mux_lastindex_rom")
			rm "/tmp/mux_lastindex_rom"
		else
			LAST_INDEX_ROM=0
		fi
	fi

	if [ "$(cat $ACT_GO)" = syslist ] && [ -s "/tmp/mux_lastindex_sys" ]; then
		LAST_INDEX_SYS=$(busybox cat "/tmp/mux_lastindex_sys")
		rm "/tmp/mux_lastindex_sys"
	else
		LAST_INDEX_SYS=0
	fi

	# muX Programs
	if [ -s "$ACT_GO" ]; then
		case "$(cat $ACT_GO)" in
			"launcher")
				echo launcher > $ACT_GO
				/system/data/extra/muxlaunch
				;;
			"syslist")
				echo launcher > $ACT_GO
				echo "$LAST_INDEX_SYS" > /tmp/lisys
				echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
				/system/data/extra/muxsyslist -i "$LAST_INDEX_SYS" -m "$MSG_SUPPRESS"
				;;
			"romlist")
				echo syslist > $ACT_GO
				echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
				/system/data/extra/muxromlist -i "$LAST_INDEX_ROM" -m "$MSG_SUPPRESS" -s "$SYSTEM" -t "$CARD"
				;;
			"setting")
				echo launcher > $ACT_GO
				/system/data/extra/muxsettings
				;;
			"tweak")
				echo setting > $ACT_GO
				/system/data/extra/muxtweak
				;;
			"rtc")
				echo setting > $ACT_GO
				/system/data/extra/muxrtc
				;;
			"import")
				echo setting > $ACT_GO
				/system/data/extra/muximport
				;;
			"tracker")
				echo setting > $ACT_GO
				/system/data/extra/muxtracker
				;;
			"sdcard")
				echo setting > $ACT_GO
				/system/data/extra/muxsdtool
				;;
			"tester")
				echo setting > $ACT_GO
				/system/data/extra/muxtester
				;;
			"bios")
				echo setting > $ACT_GO
				/system/data/extra/muxbioscheck
				;;
			"backup")
				echo setting > $ACT_GO
				/system/data/extra/muxbackup
				;;
			"reset")
				echo setting > $ACT_GO
				/system/data/extra/muxreset
				;;
			"system")
				echo setting > $ACT_GO
				/system/data/extra/muxsysinfo
				;;
			"profile")
				echo launcher > $ACT_GO
				/system/data/extra/muxprofile
				;;
			"favourite")
				echo launcher > $ACT_GO
				echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
				/system/data/extra/muxfavourite -i "$LAST_INDEX_ROM" -m "$MSG_SUPPRESS"
				if [ -s "/tmp/mux_reload" ]; then
					if [ "$(cat /tmp/mux_reload)" = 1 ]; then
						echo favourite > $ACT_GO
					fi
					rm "/tmp/mux_reload"
				fi
				;;
			"history")
				echo launcher > $ACT_GO
				echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
				/system/data/extra/muxhistory -m "$MSG_SUPPRESS"
				if [ -s "/tmp/mux_reload" ]; then
					if [ "$(cat /tmp/mux_reload)" = 1 ]; then
						echo history > $ACT_GO
					fi
					rm "/tmp/mux_reload"
				fi
				;;
			"shuffle")
				echo launcher > $ACT_GO
				/system/data/extra/muxshuffle
				;;
			"reboot")
				echo launcher > $ACT_GO
				sync && reboot
				;;
			"shutdown")
				echo launcher > $ACT_GO
				sync && busybox poweroff -f
				;;
		esac
	fi
done
