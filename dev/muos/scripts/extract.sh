#!/system/bin/sh

if [ $# -eq 0 ]; then
	echo "Usage: $0 <archive_file> <message> <pipe>"
	exit 1
fi

ARCHIVE_FILE=$1
MESSAGE=$2
PIPE=$3

FILES=$(7za l -bso0 -bse0 -bsp2 "$ARCHIVE_FILE" | busybox awk '{print $5}' | busybox tail -n1)
FOLDERS=$(7za l -bso0 -bse0 -bsp2 "$ARCHIVE_FILE" | busybox awk '{print $7}' | busybox tail -n1)
TOTAL=$((FILES + FOLDERS))

7za x -aoa -bb3 "$ARCHIVE_FILE" > /tmp/7zstatus &

while true; do
	COUNT=$(busybox grep -c '^\- ' /tmp/7zstatus)
	PERCENTAGE=$((COUNT * 100 / TOTAL))

	echo "$MESSAGE ($PERCENTAGE%)" > "$PIPE"

	busybox sleep 1

	if [ $PERCENTAGE -ge 100 ]; then
		break
	fi
done

rm /tmp/7zstatus
