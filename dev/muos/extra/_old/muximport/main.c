#include "../lvgl/lvgl.h"
#include "../lvgl/drivers/display/fbdev.h"
#include "../lvgl/drivers/indev/evdev.h"
#include "ui.h"
#include <unistd.h>
#include <pthread.h>
#include <fcntl.h>
#include <linux/joystick.h>
#include <stdio.h>
#include <stdlib.h>
#include "../common/common.h"
#include "../common/options.h"

static int js_fd;

int turbo_mode = 0;
int msgbox_active = 0;
int input_disable = 0;
int SD2_found = 0;
int safe_quit = 0;
char* osd_message = "";

// Place as many NULL as there are options!
lv_obj_t* labels[] = { NULL };
unsigned int label_count = sizeof(labels) / sizeof(labels[0]);

lv_obj_t* msgbox_element = NULL;

int force_total, force_current;

typedef struct {
    int* total;
    int* current;
} Import;

Import force;

const char* force_options[] = { "off", "on" };

void init_pointers(Import *import, int *total, int *current) {
    import->total = total;
    import->current = current;
}

static void dropdown_event_handler(lv_event_t* e) {
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* obj = lv_event_get_target(e);

    if (code == LV_EVENT_VALUE_CHANGED) {
        char buf[MAX_BUFFER_SIZE];
        lv_dropdown_get_selected_str(obj, buf, sizeof(buf));
    }
}

void elements_events_init() {
    lv_obj_t* dropdowns[] = {
            ui_droForce
    };

    for (unsigned int i = 0; i < sizeof(dropdowns) / sizeof(dropdowns[0]); i++) {
        lv_obj_add_event_cb(dropdowns[i], dropdown_event_handler, LV_EVENT_ALL, NULL);
    }

    lv_dropdown_set_dir(ui_droForce, LV_DIR_LEFT);

    init_pointers(&force, &force_total, &force_current);
}

void init_dropdown_settings() {
    Import settings[] = {
            {force.total, force.current}
    };

    lv_obj_t* dropdowns[] = {
            ui_droForce
    };

    for (unsigned int i = 0; i < sizeof(settings) / sizeof(settings[0]); i++) {
        *(settings[i].total) = lv_dropdown_get_option_cnt(dropdowns[i]);
        *(settings[i].current) = lv_dropdown_get_selected(dropdowns[i]);
    }
}

void update_retroarch_playlist(int force_update) {
    turbo_mode = 1;
    input_disable = 1;

    char line[MAX_BUFFER_SIZE];
    char command[MAX_BUFFER_SIZE];

    const char* compat = "/system/data/compat/compat.sh";
    if (force_update == 1) {
        snprintf(command, sizeof(command), "%s force", compat);
    } else {
        snprintf(command, sizeof(command), "%s", compat);
    }

    FILE* pipe = popen(command, "r");

    if (!pipe) {
        perror("popen");
        turbo_mode = 0;
        input_disable = 0;
        osd_message = "Could not update!";
        return;
    }

    lv_label_set_text(ui_lblMessage, "Input currently disabled...");

    while (fgets(line, sizeof(line), pipe) != NULL) {
        lv_textarea_add_text(ui_txtLogger, line);
    }

    lv_label_set_text(ui_lblMessage, "");

    osd_message = "Update complete!";

    pclose(pipe);

    turbo_mode = 0;
    input_disable = 0;
}

lv_group_t* update_group;

void init_navigation_groups() {
    lv_obj_t* update_objects[] = {
        ui_lblForce,
        ui_lblUpdate
    };

    labels[0] = ui_droForce;

    update_group = lv_group_create();

    for (unsigned int i = 0; i < sizeof(update_objects) / sizeof(update_objects[0]); i++) {
        lv_group_add_obj(update_group, update_objects[i]);
    }
}

void* joystick_task() {
    struct js_event ev;

    while (1) {
        if (input_disable == 1) {
            continue;
        }
        read(js_fd, &ev, sizeof(struct js_event));
        struct _lv_obj_t *element_focused = lv_group_get_focused(update_group);
        switch (ev.type) {
            case JS_EVENT_BUTTON:
                if (ev.value == 1 && ev.number == JOY_SELECT && msgbox_active == 1) {
                    msgbox_active = 0;
                    lv_obj_add_flag(msgbox_element, LV_OBJ_FLAG_HIDDEN);
                    break;
                } else {
                    if (msgbox_active == 1) {
                        break;
                    }
                    if (ev.value == 1) {
                        if (ev.number == JOY_SELECT) {
                            if (element_focused == ui_lblForce) {
                                show_help_msgbox(ui_pnlHelp,
                                    ui_lblHelpHeader,
                                    ui_lblHelpContent,
                                   "Delete Existing Playlists",
                                   "This removes the associated playlist and database of the scanned system before updating ensuring that it's up to date. Do this if you are having issues updating normally!");
                            }
                            else if (element_focused == ui_lblUpdate) {
                                show_help_msgbox(ui_pnlHelp,
                                     ui_lblHelpHeader,
                                     ui_lblHelpContent,
                                     "Update Playlists",
                                     "Scans your current ROM list and updates any playlists and database files for RetroArch to use.");
                            }
                            break;
                        }
                        else if (ev.number == JOY_A) {
                            if (element_focused == ui_lblUpdate) {
                                update_retroarch_playlist(force_current);
                            }
                            break;
                        }
                        else if (ev.number == JOY_B) {
                            safe_quit = 1;
                        }
                    }
                }
                break;
            case JS_EVENT_AXIS:
                if (msgbox_active == 1) {
                    break;
                }
                if (ev.number == JOY_LEFT && ev.value == -32767) {
                    if (element_focused == ui_lblForce) {
                        decrease_option_value(ui_droForce, &force_current, force_total);
                    }
                    break;
                }
                else if (ev.number == JOY_RIGHT && ev.value == 32767) {
                    if (element_focused == ui_lblForce) {
                        increase_option_value(ui_droForce, &force_current, force_total);
                    }
                    break;
                }
                else if (ev.number == JOY_UP && ev.value == -32767) {
                    nav_prev(update_group, 1);
                    if (element_focused == ui_lblForce) {
                        change_highlight(NULL);
                    }
                    else if (element_focused == ui_lblUpdate) {
                        change_highlight(ui_droForce);
                    }
                    break;
                }
                else if (ev.number == JOY_DOWN && ev.value == 32767) {
                    nav_next(update_group, 1);
                    if (element_focused == ui_lblForce) {
                        change_highlight(NULL);
                    }
                    break;
                }
                break;
            default:
                break;
        }
    }
}

int main() {
    setenv("PATH", "/bin:/sbin:/usr/bin:/usr/sbin:/system/bin", 1);
    setenv("NO_COLOR", "1", 1);

    lv_init();
    fbdev_init();

    static lv_color_t buf[DISP_BUF_SIZE];
    static lv_disp_draw_buf_t disp_buf;

    lv_disp_draw_buf_init(&disp_buf, buf, NULL, DISP_BUF_SIZE);

    static lv_disp_drv_t disp_drv;
    lv_disp_drv_init(&disp_drv);
    disp_drv.draw_buf = &disp_buf;
    disp_drv.flush_cb = fbdev_flush;
    disp_drv.hor_res = 640;
    disp_drv.ver_res = 480;
    lv_disp_drv_register(&disp_drv);

    ui_init();

    init_navigation_groups();
    elements_events_init();

    init_dropdown_settings();
    change_highlight(ui_droForce);

    struct dt_task_param dt_par;
    struct osd_task_param osd_par;

    dt_par.lblDatetime = ui_lblDatetime;
    dt_par.lblCapacity = ui_lblCapacity;
    osd_par.lblMessage = ui_lblMessage;

    js_fd = open(JOY_DEVICE, O_RDONLY);
    if (js_fd < 0) {
        perror("Failed to open joystick device");
        return 1;
    }

    pthread_t joystick_thread;
    pthread_t datetime_thread;
    pthread_t turbo_thread;
    pthread_t osd_thread;

    pthread_create(&joystick_thread, NULL, (void* (*)(void*))joystick_task, NULL);
    pthread_create(&datetime_thread, NULL, (void* (*)(void*))datetime_task, &dt_par);
    pthread_create(&turbo_thread, NULL, (void* (*)(void*))turbo_task, NULL);
    pthread_create(&osd_thread, NULL, (void* (*)(void*))osd_task, &osd_par);

    lv_indev_drv_t indev_drv;
    lv_indev_drv_init(&indev_drv);

    indev_drv.type = LV_INDEV_TYPE_KEYPAD;
    indev_drv.read_cb = evdev_read;
    indev_drv.user_data = (void*)(intptr_t)js_fd;

    lv_indev_drv_register(&indev_drv);

    while (!safe_quit) {
        lv_task_handler();
        usleep(UINT8_MAX);
    }

    pthread_cancel(joystick_thread);
    pthread_cancel(datetime_thread);
    pthread_cancel(turbo_thread);
    pthread_cancel(osd_thread);

    close(js_fd);

    return 0;
}

uint32_t custom_tick_get(void) {
    static uint64_t start_ms = 0;

    if (start_ms == 0) {
        struct timeval tv_start;
        gettimeofday(&tv_start, NULL);
        start_ms = (tv_start.tv_sec * 1000000 + tv_start.tv_usec) / 1000;
    }

    struct timeval tv_now;
    gettimeofday(&tv_now, NULL);

    uint64_t now_ms;
    now_ms = (tv_now.tv_sec * 1000000 + tv_now.tv_usec) / 1000;

    uint32_t time_ms = now_ms - start_ms;
    return time_ms;
}
