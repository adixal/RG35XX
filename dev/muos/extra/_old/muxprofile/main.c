#include "../lvgl/lvgl.h"
#include "../lvgl/drivers/display/fbdev.h"
#include "../lvgl/drivers/indev/evdev.h"
#include "ui.h"
#include <unistd.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <linux/input.h>
#include <linux/joystick.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define JOY_A       0
#define JOY_B       1
#define JOY_X       2
#define JOY_Y       3
#define JOY_LEFT	6
#define JOY_RIGHT	6
#define JOY_SELECT  7

#define MAX_OPTION_PROF 9
#define MAX_BUFFER_SIZE 256
#define DISP_BUF_SIZE	(640 * 480 * 32) / 8

#ifdef GARLICOS
#define CONFIG_FILE "/mnt/mmc/CFW/profile/config.txt"
#define RUNNER_FILE "/mnt/mmc/CFW/profile/runner.sh"
#else
#define CONFIG_FILE "/mnt/mmc/MUOS/profile/config.txt"
#define RUNNER_FILE "/mnt/mmc/MUOS/profile/runner.sh"
#endif

#ifdef GARLICOS
#define PNG_DIR "/mnt/mmc/CFW/profile"
#else
#define PNG_DIR "/mnt/mmc/MUOS/profile"
#endif
#define PNG_P1  "one.png"
#define PNG_P2  "two.png"
#define PNG_P3  "three.png"

static int js_fd;
static lv_obj_t* active_screen = NULL;
static lv_group_t* profile_group;

int msgbox_active = 0;

char title[24];
char col_title[8];
char col_back[8];
char col_select[8];
char col_datetime[8];

struct Profile {
    char info_a[256];
    char info_b[256];
    char info_x[256];
    char info_y[256];
    char run_a[256];
    char run_b[256];
    char run_x[256];
    char run_y[256];
    int hidden;
};
struct Profile profile[3];

char* get_config_value(const char* key) {
    FILE* file = fopen(CONFIG_FILE, "r");
    if (file == NULL) {
        fprintf(stderr, "Error: Could not open %s for reading.\n", CONFIG_FILE);
        exit(1);
    }

    char line[MAX_BUFFER_SIZE];
    char* value = NULL;

    while (fgets(line, sizeof(line), file) != NULL) {
        char* trimmed_line = strtok(line, "\t\r\n");

        if (trimmed_line != NULL && strncmp(trimmed_line, key, strlen(key)) == 0) {
            char* equals_sign = strchr(trimmed_line, '=');

            if (equals_sign != NULL) {
                value = strdup(equals_sign + 1);
                break;
            }
        }
    }

    fclose(file);
    return value;
}

void parse_profile(struct Profile* profile, int number) {
    char pf_fields[MAX_OPTION_PROF][MAX_BUFFER_SIZE] = {
        "P%d_RUN_A", "P%d_INFO_A",
        "P%d_RUN_B", "P%d_INFO_B",
        "P%d_RUN_X", "P%d_INFO_X",
        "P%d_RUN_Y", "P%d_INFO_Y",
        "P%d_HIDDEN"
    };
    struct Profile* current_profile = &profile[number];

    for (int i = 0; i < MAX_OPTION_PROF; i++) {
        char pf_buffer[MAX_BUFFER_SIZE];
        snprintf(pf_buffer, sizeof(pf_buffer), pf_fields[i], number + 1);
        const char* value = get_config_value(pf_buffer);

        switch (i) {
            case 0: // RUN A
                strncpy(current_profile->run_a, value, sizeof(current_profile->run_a) - 1);
                current_profile->run_a[sizeof(current_profile->run_a) - 1] = '\0';
                break;
            case 1: // INFO A
                strncpy(current_profile->info_a, value, sizeof(current_profile->info_a) - 1);
                current_profile->info_a[sizeof(current_profile->info_a) - 1] = '\0';
                break;
            case 2: // RUN B
                strncpy(current_profile->run_b, value, sizeof(current_profile->run_b) - 1);
                current_profile->run_b[sizeof(current_profile->run_b) - 1] = '\0';
                break;
            case 3: // INFO B
                strncpy(current_profile->info_b, value, sizeof(current_profile->info_b) - 1);
                current_profile->info_b[sizeof(current_profile->info_b) - 1] = '\0';
                break;
            case 4: // RUN X
                strncpy(current_profile->run_x, value, sizeof(current_profile->run_x) - 1);
                current_profile->run_x[sizeof(current_profile->run_x) - 1] = '\0';
                break;
            case 5: // INFO X
                strncpy(current_profile->info_x, value, sizeof(current_profile->info_x) - 1);
                current_profile->info_x[sizeof(current_profile->info_x) - 1] = '\0';
                break;
            case 6: // RUN Y
                strncpy(current_profile->run_y, value, sizeof(current_profile->run_y) - 1);
                current_profile->run_y[sizeof(current_profile->run_y) - 1] = '\0';
                break;
            case 7: // INFO Y
                strncpy(current_profile->info_y, value, sizeof(current_profile->info_y) - 1);
                current_profile->info_y[sizeof(current_profile->info_y) - 1] = '\0';
                break;
            case 8: // HIDDEN
                current_profile->hidden = (strlen(value) > 0) ? atoi(value) : -1;
                break;
        }
    }
}

uint32_t hexStringToInt(const char *hexcode) {
    char striphex[8];
    int index = (hexcode[0] == '#') ? 1 : 0;

    strncpy(striphex, hexcode + index, 6);
    striphex[6] = '\0';

    uint32_t result = 0;
    sscanf(striphex, "%x", &result);
    return result;
}

void load_profile_images() {
    char png_source[MAX_BUFFER_SIZE];

    for (int i = 0; i < 3; i++) {
        switch (i) {
            case 0:
                snprintf(png_source, sizeof(png_source), "%s/%s", PNG_DIR, PNG_P1);
                if (access(png_source, F_OK) != -1) {
                    lv_obj_set_style_bg_img_src(ui_pnlProfileOne, "P:one.png", LV_PART_MAIN | LV_STATE_DEFAULT);
                }
                break;
            case 1:
                snprintf(png_source, sizeof(png_source), "%s/%s", PNG_DIR, PNG_P2);
                if (access(png_source, F_OK) != -1) {
                    lv_obj_set_style_bg_img_src(ui_pnlProfileTwo, "P:two.png", LV_PART_MAIN | LV_STATE_DEFAULT);
                }
                break;
            case 2:
                snprintf(png_source, sizeof(png_source), "%s/%s", PNG_DIR, PNG_P3);
                if (access(png_source, F_OK) != -1) {
                    lv_obj_set_style_bg_img_src(ui_pnlProfileThree, "P:three.png", LV_PART_MAIN | LV_STATE_DEFAULT);
                }
                break;
        }
    }
}

void check_hidden_profiles() {
    for (int i = 0; i < 3; i++) {
        switch (i) {
            case 0:
                if (profile[0].hidden == 1) {
                    lv_obj_add_flag(ui_pnlProfileOne, LV_OBJ_FLAG_HIDDEN);
                }
                break;
            case 1:
                if (profile[1].hidden == 1) {
                    lv_obj_add_flag(ui_pnlProfileTwo, LV_OBJ_FLAG_HIDDEN);
                }
                break;
            case 2:
                if (profile[2].hidden == 1) {
                    lv_obj_add_flag(ui_pnlProfileThree, LV_OBJ_FLAG_HIDDEN);
                }
                break;
        }
    }
}

void generate_command_script(const char* script_name, const char* script_content, int script_profile) {
    FILE* script_file = fopen(script_name, "w");

    if (script_file == NULL) {
        perror("Error opening script file");
        return;
    }

    fprintf(script_file, "#!/system/bin/sh\n\n");
    fprintf(script_file, "/mnt/mmc/MUOS/profile/profile.sh P%d\n\n", script_profile);
    fprintf(script_file, "%s", script_content);
    fprintf(script_file, "\n\nrm -- \"$0\"");  // Self destructing! :)

    fclose(script_file);

    if (chmod(script_name, S_IRWXU | S_IRWXG | S_IRWXO) != 0) {
        perror("Error making command script executable");
        return;
    }
}

void nav_prev(lv_group_t* group, int count) {
    int i;
    for (i = 0; i < count; i++) {
        lv_group_focus_prev(group);
    }
}

void nav_next(lv_group_t* group, int count) {
    int i;
    for (i = 0; i < count; i++) {
        lv_group_focus_next(group);
    }
}

void init_navigation_groups() {
    lv_obj_t* profile_objects[] = {
        ui_pnlProfileOne,
        ui_pnlProfileTwo,
        ui_pnlProfileThree
    };

    profile_group = lv_group_create();

    for (int i = 0; i < sizeof(profile_objects) / sizeof(profile_objects[0]); i++) {
        lv_group_add_obj(profile_group, profile_objects[i]);
    }
}

void check_profile_dir(const char* profile_dir) {
    struct stat st;
    if (stat(profile_dir, &st) == 0) {
        if (S_ISDIR(st.st_mode)) {
            perror("Directory already exists");
        }
    } else {
        if (mkdir(profile_dir, 0777) != 0) {
            perror("Error creating directory");
        }
    }
}

static void joystick_task(struct Profile* profile) {
    struct js_event ev;

    while (1) {
        read(js_fd, &ev, sizeof(struct js_event));
        switch (ev.type) {
            case JS_EVENT_BUTTON:
                if (ev.number == JOY_SELECT && ev.value == 1) {
                    if (msgbox_active == 0) {
                        msgbox_active = 1;
                        if (active_screen == ui_scrProfile) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(profile_group);
                            if (element_focused == ui_pnlProfileOne) {
                                lv_label_set_text(ui_lblProfileHelpValueA, profile[0].info_a);
                                lv_label_set_text(ui_lblProfileHelpValueB, profile[0].info_b);
                                lv_label_set_text(ui_lblProfileHelpValueX, profile[0].info_x);
                                lv_label_set_text(ui_lblProfileHelpValueY, profile[0].info_y);
                            }
                            else if (element_focused == ui_pnlProfileTwo) {
                                lv_label_set_text(ui_lblProfileHelpValueA, profile[1].info_a);
                                lv_label_set_text(ui_lblProfileHelpValueB, profile[1].info_b);
                                lv_label_set_text(ui_lblProfileHelpValueX, profile[1].info_x);
                                lv_label_set_text(ui_lblProfileHelpValueY, profile[1].info_y);
                            }
                            else if (element_focused == ui_pnlProfileThree) {
                                lv_label_set_text(ui_lblProfileHelpValueA, profile[2].info_a);
                                lv_label_set_text(ui_lblProfileHelpValueB, profile[2].info_b);
                                lv_label_set_text(ui_lblProfileHelpValueX, profile[2].info_x);
                                lv_label_set_text(ui_lblProfileHelpValueY, profile[2].info_y);
                            }
                        }
                        lv_obj_clear_flag(ui_pnlProfileHelpMessage, LV_OBJ_FLAG_HIDDEN);
                        break;
                    } else {
                        msgbox_active = 0;
                        lv_obj_add_flag(ui_pnlProfileHelpMessage, LV_OBJ_FLAG_HIDDEN);
                        break;
                    }
                }
                if (ev.number == JOY_A && ev.value == 1 && msgbox_active == 0) {
                    if (active_screen == ui_scrProfile) {
                        struct _lv_obj_t *element_focused = lv_group_get_focused(profile_group);
                        if (element_focused == ui_pnlProfileOne) {
                            generate_command_script(RUNNER_FILE, profile[0].run_a, 1);
                        }
                        else if (element_focused == ui_pnlProfileTwo) {
                            generate_command_script(RUNNER_FILE, profile[1].run_a, 2);
                        }
                        else if (element_focused == ui_pnlProfileThree) {
                            generate_command_script(RUNNER_FILE, profile[2].run_a, 3);
                        }
                        lv_obj_clear_flag(ui_pnlProfileLoadMessage, LV_OBJ_FLAG_HIDDEN);
                        usleep(250000);
                        exit(0);
                    }
                    break;
                }
                if (ev.number == JOY_B && ev.value == 1 && msgbox_active == 0) {
                    if (active_screen == ui_scrProfile) {
                        struct _lv_obj_t *element_focused = lv_group_get_focused(profile_group);
                        if (element_focused == ui_pnlProfileOne) {
                            generate_command_script(RUNNER_FILE, profile[0].run_b, 1);
                        }
                        else if (element_focused == ui_pnlProfileTwo) {
                            generate_command_script(RUNNER_FILE, profile[1].run_b, 2);
                        }
                        else if (element_focused == ui_pnlProfileThree) {
                            generate_command_script(RUNNER_FILE, profile[2].run_b, 3);
                        }
                        lv_obj_clear_flag(ui_pnlProfileLoadMessage, LV_OBJ_FLAG_HIDDEN);
                        usleep(250000);
                        exit(0);
                    }
                    break;
                }
                if (ev.number == JOY_X && ev.value == 1 && msgbox_active == 0) {
                    if (active_screen == ui_scrProfile) {
                        struct _lv_obj_t *element_focused = lv_group_get_focused(profile_group);
                        if (element_focused == ui_pnlProfileOne) {
                            generate_command_script(RUNNER_FILE, profile[0].run_x, 1);
                        }
                        else if (element_focused == ui_pnlProfileTwo) {
                            generate_command_script(RUNNER_FILE, profile[1].run_x, 2);
                        }
                        else if (element_focused == ui_pnlProfileThree) {
                            generate_command_script(RUNNER_FILE, profile[2].run_x, 3);
                        }
                        lv_obj_clear_flag(ui_pnlProfileLoadMessage, LV_OBJ_FLAG_HIDDEN);
                        usleep(250000);
                        exit(0);
                    }
                    break;
                }
                if (ev.number == JOY_Y && ev.value == 1 && msgbox_active == 0) {
                    if (active_screen == ui_scrProfile) {
                        struct _lv_obj_t *element_focused = lv_group_get_focused(profile_group);
                        if (element_focused == ui_pnlProfileOne) {
                            generate_command_script(RUNNER_FILE, profile[0].run_y, 1);
                        }
                        else if (element_focused == ui_pnlProfileTwo) {
                            generate_command_script(RUNNER_FILE, profile[1].run_y, 2);
                        }
                        else if (element_focused == ui_pnlProfileThree) {
                            generate_command_script(RUNNER_FILE, profile[2].run_y, 3);
                        }
                        lv_obj_clear_flag(ui_pnlProfileLoadMessage, LV_OBJ_FLAG_HIDDEN);
                        usleep(250000);
                        exit(0);
                    }
                    break;
                }
            case JS_EVENT_AXIS:
                if (ev.number == JOY_LEFT && ev.value == -32767 && msgbox_active == 0) {
                    if (active_screen == ui_scrProfile) {
                        struct _lv_obj_t *element_focused = lv_group_get_focused(profile_group);
                        if (element_focused == ui_pnlProfileTwo || element_focused == ui_pnlProfileThree) {
                            nav_prev(profile_group, 1);
                            break;
                        }
                        break;
                    }
                }
                else if (ev.number == JOY_RIGHT && ev.value == 32767 && msgbox_active == 0) {
                    if (active_screen == ui_scrProfile) {
                        struct _lv_obj_t *element_focused = lv_group_get_focused(profile_group);
                        if (element_focused == ui_pnlProfileOne || element_focused == ui_pnlProfileTwo) {
                            nav_next(profile_group, 1);
                            break;
                        }
                        break;
                    }
                }
                break;
            default:
                break;
        }
    }
}

void update_datetime_placeholder(lv_obj_t *label) {
    time_t now = time(NULL);
    struct tm *timeinfo = localtime(&now);

    char datetime_str[128];
    strftime(datetime_str, sizeof(datetime_str), "%a %b %e %Y - %I:%M %P", timeinfo);

    lv_label_set_text(label, datetime_str);
}

static void datetime_task() {
    lv_obj_t *labels[] = {
        ui_lblProfileDatetime
    };

    while (1) {
        for (size_t i = 0; i < sizeof(labels) / sizeof(labels[0]); i++) {
            update_datetime_placeholder(labels[i]);
        }

        sleep(20);
    }
}

int main(int argc, char *argv[]) {
    setenv("PATH", "/bin:/sbin:/usr/bin:/usr/sbin:/system/bin", 1);
    setenv("NO_COLOR", "1", 1);

    check_profile_dir(PNG_DIR);

    strcpy(title, get_config_value("TITLE"));
    strcpy(col_title, get_config_value("COL_TITLE"));
    strcpy(col_back, get_config_value("COL_BACK"));
    strcpy(col_select, get_config_value("COL_SELECT"));
    strcpy(col_datetime, get_config_value("COL_DATETIME"));

    for (int i = 0; i < 3; i++) {
        parse_profile(profile, i);
    }

    lv_init();
    fbdev_init();

    static lv_color_t buf[DISP_BUF_SIZE];
    static lv_disp_draw_buf_t disp_buf;

    lv_disp_draw_buf_init(&disp_buf, buf, NULL, DISP_BUF_SIZE);

    static lv_disp_drv_t disp_drv;
    lv_disp_drv_init(&disp_drv);
    disp_drv.draw_buf = &disp_buf;
    disp_drv.flush_cb = fbdev_flush;
    disp_drv.hor_res = 640;
    disp_drv.ver_res = 480;
    lv_disp_drv_register(&disp_drv);

    ui_init();

    lv_label_set_text(ui_lblProfileTitle, title);

    uint32_t hex_title = hexStringToInt(col_title);
    lv_obj_set_style_text_color(ui_lblProfileTitle, lv_color_hex(hex_title), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_color(ui_lblProfileHelpHeader, lv_color_hex(hex_title), LV_PART_MAIN | LV_STATE_DEFAULT);

    uint32_t hex_back = hexStringToInt(col_back);
    lv_obj_set_style_bg_color(ui_scrProfile, lv_color_hex(hex_back), LV_PART_MAIN | LV_STATE_DEFAULT);

    uint32_t hex_datetime = hexStringToInt(col_datetime);
    lv_obj_set_style_text_color(ui_lblProfileDatetime, lv_color_hex(hex_datetime), LV_PART_MAIN | LV_STATE_DEFAULT);

    uint32_t hex_select = hexStringToInt(col_select);
    lv_obj_set_style_outline_color(ui_pnlProfileOne, lv_color_hex(hex_select), LV_PART_MAIN | LV_STATE_FOCUSED);
    lv_obj_set_style_outline_color(ui_pnlProfileTwo, lv_color_hex(hex_select), LV_PART_MAIN | LV_STATE_FOCUSED);
    lv_obj_set_style_outline_color(ui_pnlProfileThree, lv_color_hex(hex_select), LV_PART_MAIN | LV_STATE_FOCUSED);

    load_profile_images();
    check_hidden_profiles();

    init_navigation_groups();

    js_fd = open("/dev/input/js0", O_RDONLY);
    if (js_fd < 0) {
        perror("Failed to open joystick device");
        return 1;
    }

    pthread_t joystick_thread;
    pthread_create(&joystick_thread, NULL, (void*(*)(void*))joystick_task, profile);

    pthread_t datetime_thread;
    pthread_create(&datetime_thread, NULL, (void*(*)(void*))datetime_task, NULL);

    lv_indev_drv_t indev_drv;
    lv_indev_drv_init(&indev_drv);

    indev_drv.type = LV_INDEV_TYPE_KEYPAD;
    indev_drv.read_cb = evdev_read;
    indev_drv.user_data = (void*)(intptr_t)js_fd;

    lv_indev_drv_register(&indev_drv);

    lv_scr_load(ui_scrProfile);

    active_screen = lv_scr_act();

    while (1) {
        lv_task_handler();
        usleep(5000);
    }

    pthread_cancel(joystick_thread);
    pthread_cancel(datetime_thread);

    close(js_fd);
    return 0;
}

uint32_t custom_tick_get(void) {
    static uint64_t start_ms = 0;

    if (start_ms == 0) {
        struct timeval tv_start;
        gettimeofday(&tv_start, NULL);
        start_ms = (tv_start.tv_sec * 1000000 + tv_start.tv_usec) / 1000;
    }

    struct timeval tv_now;
    gettimeofday(&tv_now, NULL);

    uint64_t now_ms;
    now_ms = (tv_now.tv_sec * 1000000 + tv_now.tv_usec) / 1000;

    uint32_t time_ms = now_ms - start_ms;
    return time_ms;
}
