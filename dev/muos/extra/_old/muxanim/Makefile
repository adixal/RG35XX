ARCH = -marm -mtune=cortex-a9 -mfpu=neon-vfpv3 -mfloat-abi=softfp -march=armv7-a

TARGET = ${shell basename $$(pwd)}

CC = $(CROSS_COMPILE)gcc -static
ST = $(CROSS_COMPILE)strip -S --strip-unneeded --remove-section=.note.gnu.gold-version --remove-section=.comment --remove-section=.note --remove-section=.note.gnu.build-id --remove-section=.note.ABI-tag

CFLAGS  = -O3 $(ARCH) -flto=auto -ffunction-sections -fdata-sections -flto -finline-functions -Wall -Wno-implicit-function-declaration
LDFLAGS = $(CFLAGS) -lpthread -Wl,--gc-sections -s

EXTRA = $(LDFLAGS) -fno-exceptions -fno-stack-protector -fomit-frame-pointer -fno-unroll-loops -fmerge-all-constants -fno-ident -ffast-math -funroll-loops -falign-functions

LVGL_DIR_NAME ?= lvgl
LVGL_DIR ?= ${shell pwd}/..

SSRCS := ${shell cat filelist.txt}

MAINSRC = ./main.c

include $(LVGL_DIR)/$(LVGL_DIR_NAME)/lvgl.mk
include $(LVGL_DIR)/$(LVGL_DIR_NAME)/drivers/lv_drivers.mk

OBJEXT ?= .o

AOBJS = $(ASRCS:.S=$(OBJEXT))
COBJS = $(CSRCS:.c=$(OBJEXT))
SOBJS = $(SSRCS:.c=$(OBJEXT))

MAINOBJ = $(MAINSRC:.c=$(OBJEXT))

SRCS = $(ASRCS) $(CSRCS) $(SSRCS) $(MAINSRC)
OBJS = $(AOBJS) $(COBJS) $(SOBJS)

RED := \033[0;31m
GREEN := \033[0;32m
NC := \033[0m

all: message compile

%.o: %.c
	@$(CC) $(CFLAGS) -c $< -o $@ $(EXTRA)

message:
	@printf "Compiling objects...\n"

compile: $(MAINOBJ) $(AOBJS) $(COBJS) $(SOBJS)
	@printf "\nName:\t$(TARGET)\n"
	@$(CC) $(MAINOBJ) $(AOBJS) $(COBJS) $(SOBJS) -o $(TARGET) ../common/common.c
	@printf "< ST:\t${RED}%s${NC}\n" "$$(ls -lah $(TARGET) | awk '{print $$5}')"
	@$(ST) $(TARGET)
	@printf "> ST:\t${GREEN}%s${NC}\n\n" "$$(ls -lah $(TARGET) | awk '{print $$5}')"

clean:
	@printf "Cleaning objects and Binaries...\n"
	@rm -f $(MAINOBJ) $(AOBJS) $(COBJS) $(SOBJS) $(TARGET)
