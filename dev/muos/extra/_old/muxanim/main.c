#include "../lvgl/lvgl.h"
#include "../lvgl/drivers/display/fbdev.h"
#include "ui.h"
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <linux/joystick.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "../common/options.h"

int turbo_mode = 0;
int msgbox_active = 0;
int input_disable = 0;
int SD2_found = 0;
int safe_quit = 0;
char* osd_message = "";
char* osd_default = "";

// Place as many NULL as there are options!
lv_obj_t* labels[] = { };
unsigned int label_count = sizeof(labels) / sizeof(labels[0]);

lv_obj_t* msgbox_element = NULL;

char anim_file[1024];
char animation[1024];

char* theme;
char* message;

int anim_wait;

void* animation_task() {
    snprintf(anim_file, sizeof(anim_file), "/misc/theme/%s/anim.gif", theme);

    if (file_exist(anim_file)) {
        lv_label_set_text(ui_lblMessage, message);
        snprintf(animation, sizeof(animation), "M:../misc/theme/%s/anim.gif", theme);
        lv_gif_set_src(lv_gif_create(ui_pnlGIF), animation);
        sleep(anim_wait);
    } else {
        perror("could not find theme");
    }

    safe_quit = 1;
    return NULL;
}

int main(int argc, char *argv[]) {
    if (argc != 4) {
        printf("Usage: %s <theme> <sleep> <text>\n", argv[0]);
        return 1;
    }

    setenv("PATH", "/bin:/sbin:/usr/bin:/usr/sbin:/system/bin", 1);
    setenv("NO_COLOR", "1", 1);

    lv_init();
    fbdev_init();

    static lv_color_t buf[DISP_BUF_SIZE];
    static lv_disp_draw_buf_t disp_buf;

    lv_disp_draw_buf_init(&disp_buf, buf, NULL, DISP_BUF_SIZE);

    static lv_disp_drv_t disp_drv;
    lv_disp_drv_init(&disp_drv);
    disp_drv.draw_buf = &disp_buf;
    disp_drv.flush_cb = fbdev_flush;
    disp_drv.hor_res = 640;
    disp_drv.ver_res = 480;
    lv_disp_drv_register(&disp_drv);

    ui_init();

    theme = argv[1];
    message = argv[3];
    anim_wait = atoi(argv[2]);

    pthread_t animation_thread;
    pthread_create(&animation_thread, NULL, (void* (*)(void*))animation_task, NULL);

    while (!safe_quit) {
        lv_task_handler();
    }

    pthread_cancel(animation_thread);

    return 0;
}

uint32_t custom_tick_get(void) {
    static uint64_t start_ms = 0;

    if (start_ms == 0) {
        struct timeval tv_start;
        gettimeofday(&tv_start, NULL);
        start_ms = (tv_start.tv_sec * 1000000 + tv_start.tv_usec) / 1000;
    }

    struct timeval tv_now;
    gettimeofday(&tv_now, NULL);

    uint64_t now_ms;
    now_ms = (tv_now.tv_sec * 1000000 + tv_now.tv_usec) / 1000;

    uint32_t time_ms = now_ms - start_ms;
    return time_ms;
}
