#include "../lvgl/lvgl.h"
#include "../lvgl/drivers/display/fbdev.h"
#include "../lvgl/drivers/indev/evdev.h"
#include "ui.h"
#include <unistd.h>
#include <pthread.h>
#include <sys/epoll.h>
#include <sys/types.h>
#include <fcntl.h>
#include <linux/joystick.h>
#include <string.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <libgen.h>
#include "../common/common.h"
#include "../common/help.h"
#include "../common/options.h"
#include "../common/theme.h"
#include "../common/mini/mini.h"

static int js_fd;

int turbo_mode = 0;
int msgbox_active = 0;
int input_disable = 0;
int SD2_found = 0;
int nav_sound = 0;
int safe_quit = 0;
int bar_header = 0;
int bar_footer = 0;
char *osd_message;
mini_t *muos_config;

int nav_moved = 1;
char *current_wall = "";

// Place as many NULL as there are options!
lv_obj_t *labels[] = {NULL, NULL, NULL, NULL, NULL, NULL};
unsigned int label_count = sizeof(labels) / sizeof(labels[0]);

lv_obj_t *msgbox_element = NULL;

int progress_onscreen = -1;

int gamepad_total, gamepad_current;
int thermal_total, thermal_current;
int font_total, font_current;
int verbose_total, verbose_current;
int volume_total, volume_current;
int offset_total, offset_current;

typedef struct {
    int *total;
    int *current;
} Tweak;

Tweak gamepad, thermal, font, verbose, volume, offset;

lv_group_t *ui_group;
lv_group_t *ui_group_value;
lv_group_t *ui_group_glyph;

void show_help(lv_obj_t *element_focused) {
    char *message = NO_HELP_FOUND;

    if (element_focused == ui_lblGamepad) {
        message = MUXTWEAKADV_GAMEPAD;
    } else if (element_focused == ui_lblThermal) {
        message = MUXTWEAKADV_THERMAL;
    } else if (element_focused == ui_lblFont) {
        message = MUXTWEAKADV_FONT;
    } else if (element_focused == ui_lblVerbose) {
        message = MUXTWEAKADV_VERBOSE;
    } else if (element_focused == ui_lblVolume) {
        message = MUXTWEAKADV_VOLUME;
    } else if (element_focused == ui_lblOffset) {
        message = MUXTWEAKADV_OFFSET;
    }

    if (strlen(message) <= 1) {
        message = NO_HELP_FOUND;
    }

    show_help_msgbox(ui_pnlHelp, ui_lblHelpHeader, ui_lblHelpContent, lv_label_get_text(element_focused), message);
}

void init_pointers(Tweak *tweak, int *total, int *current) {
    tweak->total = total;
    tweak->current = current;
}

static void dropdown_event_handler(lv_event_t *e) {
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t * obj = lv_event_get_target(e);

    if (code == LV_EVENT_VALUE_CHANGED) {
        char buf[MAX_BUFFER_SIZE];
        lv_dropdown_get_selected_str(obj, buf, sizeof(buf));
    }
}

void elements_events_init() {
    lv_obj_t *dropdowns[] = {
            ui_droGamepad,
            ui_droThermal,
            ui_droFont,
            ui_droVerbose,
            ui_droVolume,
            ui_droOffset
    };

    labels[0] = ui_droGamepad;
    labels[1] = ui_droThermal;
    labels[2] = ui_droFont;
    labels[3] = ui_droVerbose;
    labels[4] = ui_droVolume;
    labels[5] = ui_droOffset;

    for (unsigned int i = 0; i < sizeof(dropdowns) / sizeof(dropdowns[0]); i++) {
        lv_obj_add_event_cb(dropdowns[i], dropdown_event_handler, LV_EVENT_ALL, NULL);
    }

    init_pointers(&gamepad, &gamepad_total, &gamepad_current);
    init_pointers(&thermal, &thermal_total, &thermal_current);
    init_pointers(&font, &font_total, &font_current);
    init_pointers(&verbose, &verbose_total, &verbose_current);
    init_pointers(&volume, &volume_total, &volume_current);
    init_pointers(&offset, &offset_total, &offset_current);
}

void init_dropdown_settings() {
    Tweak settings[] = {
            {gamepad.total, gamepad.current},
            {thermal.total, thermal.current},
            {font.total,    font.current},
            {verbose.total, verbose.current},
            {volume.total,  volume.current},
            {offset.total,  offset.current}
    };

    lv_obj_t *dropdowns[] = {
            ui_droGamepad,
            ui_droThermal,
            ui_droFont,
            ui_droVerbose,
            ui_droVolume,
            ui_droOffset
    };

    for (unsigned int i = 0; i < sizeof(settings) / sizeof(settings[0]); i++) {
        *(settings[i].total) = lv_dropdown_get_option_cnt(dropdowns[i]);
        *(settings[i].current) = lv_dropdown_get_selected(dropdowns[i]);
    }
}

void restore_tweak_options() {
    int idx_gamepad = mini_get_int(muos_config, "tweak", "gamepad", 0);
    int idx_thermal = mini_get_int(muos_config, "tweak", "thermal", 0);
    int idx_font = mini_get_int(muos_config, "tweak", "font", 0);
    int idx_verbose = mini_get_int(muos_config, "tweak", "verbose", 0);
    int idx_volume_low = mini_get_int(muos_config, "tweak", "volume_low", 0);
    int idx_offset = mini_get_int(muos_config, "tweak", "offset", 50);

    lv_dropdown_set_selected(ui_droGamepad, idx_gamepad);
    lv_dropdown_set_selected(ui_droThermal, idx_thermal);
    lv_dropdown_set_selected(ui_droFont, idx_font);
    lv_dropdown_set_selected(ui_droVerbose, idx_verbose);
    lv_dropdown_set_selected(ui_droVolume, idx_volume_low);
    lv_dropdown_set_selected(ui_droOffset, idx_offset);
}

void save_tweak_options() {
    int idx_gamepad = lv_dropdown_get_selected(ui_droGamepad);
    int idx_thermal = lv_dropdown_get_selected(ui_droThermal);
    int idx_font = lv_dropdown_get_selected(ui_droFont);
    int idx_verbose = lv_dropdown_get_selected(ui_droVerbose);
    int idx_volume_low = lv_dropdown_get_selected(ui_droVolume);
    int idx_offset = lv_dropdown_get_selected(ui_droOffset);

    mini_set_int(muos_config, "tweak", "gamepad", idx_gamepad);
    mini_set_int(muos_config, "tweak", "thermal", idx_thermal);
    mini_set_int(muos_config, "tweak", "font", idx_font);
    mini_set_int(muos_config, "tweak", "verbose", idx_verbose);
    mini_set_int(muos_config, "tweak", "volume_low", idx_volume_low);
    mini_set_int(muos_config, "tweak", "offset", idx_offset);

    mini_save(muos_config, MINI_FLAGS_SKIP_EMPTY_GROUPS);

    run_shell_script(MUOS_TWEAK_UPDATE);
}

void init_navigation_groups() {
    lv_obj_t *ui_objects[] = {
            ui_lblGamepad,
            ui_lblThermal,
            ui_lblFont,
            ui_lblVerbose,
            ui_lblVolume,
            ui_lblOffset
    };

    lv_obj_t *ui_objects_value[] = {
            ui_droGamepad,
            ui_droThermal,
            ui_droFont,
            ui_droVerbose,
            ui_droVolume,
            ui_droOffset
    };

    lv_obj_t *ui_objects_icon[] = {
            ui_icoGamepad,
            ui_icoThermal,
            ui_icoFont,
            ui_icoVerbose,
            ui_icoVolume,
            ui_icoOffset
    };

    ui_group = lv_group_create();
    ui_group_value = lv_group_create();
    ui_group_glyph = lv_group_create();

    for (unsigned int i = 0; i < sizeof(ui_objects) / sizeof(ui_objects[0]); i++) {
        lv_group_add_obj(ui_group, ui_objects[i]);
    }

    for (unsigned int i = 0; i < sizeof(ui_objects_value) / sizeof(ui_objects_value[0]); i++) {
        lv_group_add_obj(ui_group_value, ui_objects_value[i]);
    }

    for (unsigned int i = 0; i < sizeof(ui_objects_icon) / sizeof(ui_objects_icon[0]); i++) {
        lv_group_add_obj(ui_group_glyph, ui_objects_icon[i]);
    }
}

void *joystick_task() {
    struct input_event ev;
    int epoll_fd;
    struct epoll_event event, events[MAX_EVENTS];

    int JOYHOTKEY_pressed = 0;

    epoll_fd = epoll_create1(0);
    if (epoll_fd == -1) {
        perror("Error creating EPOLL instance");
        return NULL;
    }

    event.events = EPOLLIN;
    event.data.fd = js_fd;
    if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, js_fd, &event) == -1) {
        perror("Error with EPOLL controller");
        return NULL;
    }

    while (1) {
        int num_events = epoll_wait(epoll_fd, events, MAX_EVENTS, 64);
        if (num_events == -1) {
            perror("Error with EPOLL wait event timer");
            continue;
        }

        for (int i = 0; i < num_events; i++) {
            if (events[i].data.fd == js_fd) {
                int ret = read(js_fd, &ev, sizeof(struct input_event));
                if (ret == -1) {
                    perror("Error reading input");
                    continue;
                }

                struct _lv_obj_t *element_focused = lv_group_get_focused(ui_group);
                switch (ev.type) {
                    case EV_KEY:
                        if (ev.value == 1) {
                            if (msgbox_active) {
                                switch (ev.code) {
                                    case JOY_B:
                                    case JOY_MENU:
                                        play_sound("confirm", nav_sound);
                                        msgbox_active = 0;
                                        progress_onscreen = 0;
                                        lv_obj_add_flag(msgbox_element, LV_OBJ_FLAG_HIDDEN);
                                        break;
                                    default:
                                        break;
                                }
                            } else {
                                switch (ev.code) {
                                    case JOY_A:
                                        if (element_focused == ui_lblGamepad) {
                                            increase_option_value(ui_droGamepad, &gamepad_current, gamepad_total);
                                        } else if (element_focused == ui_lblThermal) {
                                            increase_option_value(ui_droThermal, &thermal_current, thermal_total);
                                        } else if (element_focused == ui_lblFont) {
                                            increase_option_value(ui_droFont, &font_current, font_total);
                                        } else if (element_focused == ui_lblVerbose) {
                                            increase_option_value(ui_droVerbose, &verbose_current, verbose_total);
                                        } else if (element_focused == ui_lblVolume) {
                                            increase_option_value(ui_droVolume, &volume_current, volume_total);
                                        } else if (element_focused == ui_lblOffset) {
                                            increase_option_value(ui_droOffset, &offset_current, offset_total);
                                        }
                                        play_sound("navigate", nav_sound);
                                        break;
                                    case JOY_MENU:
                                        JOYHOTKEY_pressed = 1;
                                        break;
                                    case JOY_B:
                                        play_sound("back", nav_sound);
                                        input_disable = 1;

                                        osd_message = "Saving Changes";
                                        lv_label_set_text(ui_lblMessage, osd_message);
                                        lv_obj_clear_flag(ui_pnlMessage, LV_OBJ_FLAG_HIDDEN);

                                        save_tweak_options();
                                        safe_quit = 1;
                                        break;
                                }
                            }
                        } else {
                            if (ev.code == JOY_MENU) {
                                JOYHOTKEY_pressed = 0;
                                if (progress_onscreen == -1) {
                                    play_sound("confirm", nav_sound);
                                    show_help(element_focused);
                                }
                            }
                        }
                    case EV_ABS:
                        if (msgbox_active) {
                            break;
                        }
                        if (ev.code == ABS_HAT0Y || ev.code == ABS_RX) {
                            switch (ev.value) {
                                case -4096:
                                case -1:
                                    nav_prev(ui_group, 1);
                                    nav_prev(ui_group_value, 1);
                                    nav_prev(ui_group_glyph, 1);
                                    play_sound("navigate", nav_sound);
                                    nav_moved = 1;
                                    break;
                                case 1:
                                case 4096:
                                    nav_next(ui_group, 1);
                                    nav_next(ui_group_value, 1);
                                    nav_next(ui_group_glyph, 1);
                                    play_sound("navigate", nav_sound);
                                    nav_moved = 1;
                                    break;
                                default:
                                    break;
                            }
                        } else if (ev.code == ABS_HAT0X || ev.code == ABS_Z) {
                            switch (ev.value) {
                                case -4096:
                                case -1:
                                    if (element_focused == ui_lblGamepad) {
                                        decrease_option_value(ui_droGamepad, &gamepad_current, gamepad_total);
                                    } else if (element_focused == ui_lblThermal) {
                                        decrease_option_value(ui_droThermal, &thermal_current, thermal_total);
                                    } else if (element_focused == ui_lblFont) {
                                        decrease_option_value(ui_droFont, &font_current, font_total);
                                    } else if (element_focused == ui_lblVerbose) {
                                        decrease_option_value(ui_droVerbose, &verbose_current, verbose_total);
                                    } else if (element_focused == ui_lblVolume) {
                                        decrease_option_value(ui_droVolume, &volume_current, volume_total);
                                    } else if (element_focused == ui_lblOffset) {
                                        decrease_option_value(ui_droOffset, &offset_current, offset_total);
                                    }
                                    play_sound("navigate", nav_sound);
                                    break;
                                case 1:
                                case 4096:
                                    if (element_focused == ui_lblGamepad) {
                                        increase_option_value(ui_droGamepad, &gamepad_current, gamepad_total);
                                    } else if (element_focused == ui_lblThermal) {
                                        increase_option_value(ui_droThermal, &thermal_current, thermal_total);
                                    } else if (element_focused == ui_lblFont) {
                                        increase_option_value(ui_droFont, &font_current, font_total);
                                    } else if (element_focused == ui_lblVerbose) {
                                        increase_option_value(ui_droVerbose, &verbose_current, verbose_total);
                                    } else if (element_focused == ui_lblVolume) {
                                        increase_option_value(ui_droVolume, &volume_current, volume_total);
                                    } else if (element_focused == ui_lblOffset) {
                                        increase_option_value(ui_droOffset, &offset_current, offset_total);
                                    }
                                    play_sound("navigate", nav_sound);
                                    break;
                                default:
                                    break;
                            }
                        }
                    default:
                        break;
                }
            }
        }

        if (ev.type == EV_KEY && ev.value == 1 && (ev.code == JOY_MINUS || ev.code == JOY_PLUS)) {
            progress_onscreen = 1;
            if (lv_obj_has_flag(ui_pnlProgress, LV_OBJ_FLAG_HIDDEN)) {
                lv_obj_clear_flag(ui_pnlProgress, LV_OBJ_FLAG_HIDDEN);
            }
            if (JOYHOTKEY_pressed) {
                lv_label_set_text(ui_icoProgress, "\uF185");
                lv_bar_set_value(ui_barProgress, get_brightness_percentage(get_brightness()), LV_ANIM_OFF);
            } else {
                int volume = get_volume_percentage();
                switch (volume) {
                    case 0:
                        lv_label_set_text(ui_icoProgress, "\uF6A9");
                        break;
                    case 1 ... 46:
                        lv_label_set_text(ui_icoProgress, "\uF026");
                        break;
                    case 47 ... 71:
                        lv_label_set_text(ui_icoProgress, "\uF027");
                        break;
                    case 72 ... 100:
                        lv_label_set_text(ui_icoProgress, "\uF028");
                        break;
                }
                lv_bar_set_value(ui_barProgress, volume, LV_ANIM_OFF);
            }
        }

        lv_task_handler();
        usleep(SCREEN_REFRESH);
    }
}

void init_elements() {
    lv_obj_move_foreground(ui_pnlFooter);
    lv_obj_move_foreground(ui_pnlHeader);
    lv_obj_move_foreground(ui_pnlHelp);
    lv_obj_move_foreground(ui_pnlProgress);

    if (bar_footer) {
        lv_obj_set_style_bg_opa(ui_pnlFooter, 0, LV_PART_MAIN | LV_STATE_DEFAULT);
    }

    if (bar_header) {
        lv_obj_set_style_bg_opa(ui_pnlHeader, 0, LV_PART_MAIN | LV_STATE_DEFAULT);
    }

    process_visual_element("clock", ui_lblDatetime);
    process_visual_element("battery", ui_staCapacity);
    process_visual_element("network", ui_staNetwork);
    process_visual_element("bluetooth", ui_staBluetooth);

    lv_label_set_text(ui_lblMessage, osd_message);

    lv_label_set_text(ui_lblNavB, "Save");
    lv_label_set_text(ui_lblNavMenu, "Help");

    lv_obj_t *nav_hide[] = {
            ui_lblNavAGlyph,
            ui_lblNavA,
            ui_lblNavCGlyph,
            ui_lblNavC,
            ui_lblNavXGlyph,
            ui_lblNavX,
            ui_lblNavYGlyph,
            ui_lblNavY,
            ui_lblNavZGlyph,
            ui_lblNavZ
    };

    for (int i = 0; i < sizeof(nav_hide) / sizeof(nav_hide[0]); i++) {
        lv_obj_add_flag(nav_hide[i], LV_OBJ_FLAG_HIDDEN);
    }

    lv_obj_set_user_data(ui_lblGamepad, "gamepad");
    lv_obj_set_user_data(ui_lblThermal, "thermal");
    lv_obj_set_user_data(ui_lblFont, "font");
    lv_obj_set_user_data(ui_lblVerbose, "verbose");
    lv_obj_set_user_data(ui_lblVolume, "volume");
    lv_obj_set_user_data(ui_lblOffset, "offset");
}

void glyph_task() {
    // TODO: Bluetooth connectivity!

/*
    if (is_network_connected() > 0) {
        lv_obj_set_style_text_color(ui_staNetwork, lv_color_hex(theme.NETWORK.ACTIVE), LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_opa(ui_staNetwork, theme.NETWORK.ACTIVE_ALPHA, LV_PART_MAIN | LV_STATE_DEFAULT);
    }
    else {
        lv_obj_set_style_text_color(ui_staNetwork, lv_color_hex(theme.NETWORK.NORMAL), LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_opa(ui_staNetwork, theme.NETWORK.NORMAL_ALPHA, LV_PART_MAIN | LV_STATE_DEFAULT);
    }
*/

    if (atoi(read_text_from_file(BATT_CHARGER))) {
        lv_obj_set_style_text_color(ui_staCapacity, lv_color_hex(theme.BATTERY.ACTIVE),
                                    LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_opa(ui_staCapacity, theme.BATTERY.ACTIVE_ALPHA, LV_PART_MAIN | LV_STATE_DEFAULT);
    } else if (read_battery_capacity() <= 15) {
        lv_obj_set_style_text_color(ui_staCapacity, lv_color_hex(theme.BATTERY.LOW), LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_opa(ui_staCapacity, theme.BATTERY.LOW_ALPHA, LV_PART_MAIN | LV_STATE_DEFAULT);
    } else {
        lv_obj_set_style_text_color(ui_staCapacity, lv_color_hex(theme.BATTERY.NORMAL),
                                    LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_opa(ui_staCapacity, theme.BATTERY.NORMAL_ALPHA, LV_PART_MAIN | LV_STATE_DEFAULT);
    }

    if (progress_onscreen > 0) {
        progress_onscreen -= 1;
    } else {
        if (!lv_obj_has_flag(ui_pnlProgress, LV_OBJ_FLAG_HIDDEN)) {
            lv_obj_add_flag(ui_pnlProgress, LV_OBJ_FLAG_HIDDEN);
        }
        if (!msgbox_active) {
            progress_onscreen = -1;
        }
    }
}

void ui_refresh_task() {
    if (nav_moved) {
        if (lv_group_get_obj_count(ui_group) > 0) {
            static char old_wall[MAX_BUFFER_SIZE];
            static char new_wall[MAX_BUFFER_SIZE];

            snprintf(old_wall, sizeof(old_wall), "%s", current_wall);
            snprintf(new_wall, sizeof(new_wall), "%s", load_wallpaper(
                    ui_scrTweakAdvanced, ui_group, theme.MISC.ANIMATED_BACKGROUND));

            if (strcmp(new_wall, old_wall) != 0) {
                strcpy(current_wall, new_wall);
                if (strlen(new_wall) > 3) {
                    printf("LOADING WALLPAPER: %s\n", new_wall);
                    if (theme.MISC.ANIMATED_BACKGROUND) {
                        lv_obj_t * img = lv_gif_create(ui_pnlWall);
                        lv_gif_set_src(img, new_wall);
                    } else {
                        lv_img_set_src(ui_imgWall, new_wall);
                    }
                    lv_obj_invalidate(ui_pnlWall);
                } else {
                    lv_img_set_src(ui_imgWall, &ui_img_nothing_png);
                }
            }
        }
        lv_obj_invalidate(ui_pnlContent);
        lv_task_handler();
        nav_moved = 0;
    }
}

int main(int argc, char *argv[]) {
    srand(time(NULL));

    setenv("PATH", "/bin:/sbin:/usr/bin:/usr/sbin:/system/bin", 1);
    setenv("NO_COLOR", "1", 1);

    lv_init();
    fbdev_init();

    static lv_color_t buf1[DISP_BUF_SIZE];
    static lv_color_t buf2[DISP_BUF_SIZE];
    static lv_disp_draw_buf_t disp_buf;

    lv_disp_draw_buf_init(&disp_buf, buf1, buf2, DISP_BUF_SIZE);

    static lv_disp_drv_t disp_drv;
    lv_disp_drv_init(&disp_drv);
    disp_drv.draw_buf = &disp_buf;
    disp_drv.flush_cb = fbdev_flush;
    disp_drv.hor_res = SCREEN_WIDTH;
    disp_drv.ver_res = SCREEN_HEIGHT;
    lv_disp_drv_register(&disp_drv);

    ui_init();
    muos_config = mini_try_load(MUOS_CONFIG_FILE);

    init_elements();

    lv_obj_set_user_data(ui_scrTweakAdvanced, basename(argv[0]));

    lv_label_set_text(ui_lblDatetime, get_datetime());
    lv_label_set_text(ui_staCapacity, get_capacity());

    load_theme(&theme, basename(argv[0]));
    apply_theme();

    current_wall = load_wallpaper(ui_scrTweakAdvanced, NULL, theme.MISC.ANIMATED_BACKGROUND);
    if (strlen(current_wall) > 3) {
        if (theme.MISC.ANIMATED_BACKGROUND) {
            lv_obj_t * img = lv_gif_create(ui_pnlWall);
            lv_gif_set_src(img, current_wall);
        } else {
            lv_img_set_src(ui_imgWall, current_wall);
        }
    } else {
        lv_img_set_src(ui_imgWall, &ui_img_nothing_png);
    }

    load_font(basename(argv[0]), ui_scrTweakAdvanced);

    if (get_ini_int(muos_config, "tweak", "sound", LABEL) == 2) {
        nav_sound = 1;
    }

    init_navigation_groups();
    elements_events_init();

    restore_tweak_options();
    init_dropdown_settings();

    struct dt_task_param dt_par;
    struct bat_task_param bat_par;
    struct osd_task_param osd_par;

    dt_par.lblDatetime = ui_lblDatetime;
    bat_par.staCapacity = ui_staCapacity;
    osd_par.lblMessage = ui_lblMessage;
    osd_par.pnlMessage = ui_pnlMessage;
    osd_par.count = 0;

    js_fd = open(JOY_DEVICE, O_RDONLY);
    if (js_fd < 0) {
        perror("Failed to open joystick device");
        return 1;
    }

    lv_indev_drv_t indev_drv;
    lv_indev_drv_init(&indev_drv);

    indev_drv.type = LV_INDEV_TYPE_KEYPAD;
    indev_drv.read_cb = evdev_read;
    indev_drv.user_data = (void *) (intptr_t) js_fd;

    lv_indev_drv_register(&indev_drv);

    lv_timer_t *datetime_timer = lv_timer_create(datetime_task, UINT16_MAX / 2, &dt_par);
    lv_timer_ready(datetime_timer);

    lv_timer_t *capacity_timer = lv_timer_create(capacity_task, UINT16_MAX / 2, &bat_par);
    lv_timer_ready(capacity_timer);

    lv_timer_t *osd_timer = lv_timer_create(osd_task, UINT16_MAX / 32, &osd_par);
    lv_timer_ready(osd_timer);

    lv_timer_t *glyph_timer = lv_timer_create(glyph_task, UINT16_MAX / 32, NULL);
    lv_timer_ready(glyph_timer);

    lv_timer_t *ui_refresh_timer = lv_timer_create(ui_refresh_task, UINT8_MAX / 4, NULL);
    lv_timer_ready(ui_refresh_timer);

    pthread_t joystick_thread;
    pthread_create(&joystick_thread, NULL, (void *(*)(void *)) joystick_task, NULL);

    init_elements();
    while (!safe_quit) {
        usleep(SCREEN_REFRESH);
    }

    mini_free(muos_config);

    pthread_cancel(joystick_thread);

    close(js_fd);

    return 0;
}

uint32_t mux_tick(void) {
    static uint64_t start_ms = 0;

    if (start_ms == 0) {
        struct timeval tv_start;
        gettimeofday(&tv_start, NULL);
        start_ms = (tv_start.tv_sec * 1000000 + tv_start.tv_usec) / 1000;
    }

    struct timeval tv_now;
    gettimeofday(&tv_now, NULL);

    uint64_t now_ms;
    now_ms = (tv_now.tv_sec * 1000000 + tv_now.tv_usec) / 1000;

    uint32_t time_ms = now_ms - start_ms;
    return time_ms;
}
