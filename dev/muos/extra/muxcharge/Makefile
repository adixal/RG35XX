ifeq ($(DEVICE), RG35XX)
	ARCH = -marm -mtune=cortex-a9 -mfpu=neon-vfpv3 -mfloat-abi=softfp -march=armv7-a
else ifeq ($(DEVICE), RG35XXPLUS)
	ARCH = -marm -mtune=cortex-a53 -mfpu=neon-fp-armv8 -mfloat-abi=hard -march=armv8-a
else
	$(error Unsupported Device: $(DEVICE))
endif

TARGET = ${shell basename $$(pwd)}

CC = $(CROSS_COMPILE)gcc -static
ST = $(CROSS_COMPILE)strip -s -x

CFLAGS  = -O3 $(ARCH) -flto=auto -ffunction-sections -fdata-sections -flto -finline-functions -Wall -Wno-implicit-function-declaration
LDFLAGS = $(CFLAGS) -lpthread -Wl,--gc-sections -s

ifeq ($(DEVICE), RG35XXPLUS)
	LDFLAGS += -lSDL2
endif

EXTRA = $(LDFLAGS) -fno-exceptions -fno-stack-protector -fomit-frame-pointer -fno-unroll-loops -fmerge-all-constants -fno-ident -ffast-math -funroll-loops -falign-functions

LVGL_DIR_NAME ?= lvgl
LVGL_DIR ?= ${shell pwd}/..

SSRCS := ${shell cat filelist.txt}

MAINSRC = ./main.c

include $(LVGL_DIR)/$(LVGL_DIR_NAME)/lvgl.mk
include $(LVGL_DIR)/$(LVGL_DIR_NAME)/drivers/lv_drivers.mk

OBJEXT ?= .o

AOBJS = $(ASRCS:.S=$(OBJEXT))
COBJS = $(CSRCS:.c=$(OBJEXT))
SOBJS = $(SSRCS:.c=$(OBJEXT))

MAINOBJ = $(MAINSRC:.c=$(OBJEXT))

SRCS = $(ASRCS) $(CSRCS) $(SSRCS) $(MAINSRC)
OBJS = $(AOBJS) $(COBJS) $(SOBJS)

RED := \033[0;31m
GREEN := \033[0;32m
NC := \033[0m

all: message compile

%.o: %.c
	@$(CC) -D$(DEVICE) $(CFLAGS) -c $< -o $@ $(EXTRA)

message:
	@printf "Compiling objects...\n"
	@printf "\nDevice: $(DEVICE)\n"

compile: $(MAINOBJ) $(AOBJS) $(COBJS) $(SOBJS)
	@printf "Name:\t$(TARGET)\n"
	@$(CC) -D$(DEVICE) $(MAINOBJ) $(AOBJS) $(COBJS) $(SOBJS) -o $(TARGET) ../common/mini/mini.c ../common/json/json.c ../common/array.c ../common/common.c ../common/theme.c theme.c
	@printf "< ST:\t${RED}%s${NC}\n" "$$(ls -lah $(TARGET) | awk '{print $$5}')"
	@$(ST) $(TARGET)
	@printf "> ST:\t${GREEN}%s${NC}\n\n" "$$(ls -lah $(TARGET) | awk '{print $$5}')"

clean:
	@printf "Cleaning objects and Binaries...\n"
	@rm -f $(MAINOBJ) $(AOBJS) $(COBJS) $(SOBJS) $(TARGET)
