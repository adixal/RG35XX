AstroFire is a classic asteroid-blasting arcade game, featuring 3D-rendered graphics and 18 different types of enemy craft, spread over 75 action-packed levels.
