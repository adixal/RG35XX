#!/bin/sh

for DIR in */; do
    SRC=("$DIR"/*.dts)
    for DTS in "${SRC[@]}"; do
        dtc -O dtb -o $(basename "$DTS" .dts).dtb "$DTS"
    done
done
