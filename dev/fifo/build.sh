#!/bin/sh

BIN=readfifo

if [ -e "$BIN" ]; then
	rm "$BIN"
fi

. ~/crosscompile.sh
arm-linux-gcc -static -o "$BIN" main.c
arm-linux-strip -x "$BIN"
arm-linux-strip -s "$BIN"
ls -lh "$BIN"
file "$BIN"
