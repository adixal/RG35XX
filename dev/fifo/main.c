#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/epoll.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <pipe_name>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    const char *pipe_name = argv[1];

    if (access(pipe_name, F_OK) != -1) {
        printf("Pipe \"%s\" already exists.\n", pipe_name);
        exit(EXIT_FAILURE);
    } else {
        if (mkfifo(pipe_name, 0666) == -1) {
            perror("mkfifo");
            exit(EXIT_FAILURE);
        }
        printf("Pipe \"%s\" created.\n", pipe_name);
    }

    int fd = open(pipe_name, O_RDONLY);
    if (fd == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }

    char buffer[1024];
    ssize_t bytes_read;

    int epoll_fd = epoll_create1(0);
    struct epoll_event event;
    event.events = EPOLLIN;
    event.data.fd = fd;
    epoll_ctl(epoll_fd, EPOLL_CTL_ADD, fd, &event);

    while (1) {
        struct epoll_event events;
        int num_events = epoll_wait(epoll_fd, &events, 1, -1);

        if (num_events > 0) {
            if (events.events & EPOLLIN) {
                bytes_read = read(fd, buffer, sizeof(buffer));
                if (bytes_read > 0) {
                    buffer[bytes_read] = '\0';

                    if (buffer[bytes_read - 1] == '\n') {
                        buffer[bytes_read - 1] = '\0';
                    }

                    if (strcmp(buffer, "quit") == 0) {
                        printf("Quit received.\n");
                        break;
                    } else {
                        printf("%s\n", buffer);
                    }
                }
            }
        }
    }

    close(fd);
    unlink(pipe_name);

    return 0;
}
