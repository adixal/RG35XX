#include <fcntl.h>
#include <linux/fb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>

typedef struct {
    int x, y;
} Point;

typedef struct {
    int width, height;
    unsigned char* buffer;
} Framebuffer;

typedef struct {
    Point position;
    int speed;
} Star;

void updateStar(Star* star, int screenWidth, int screenHeight) {
    star->position.x += star->speed;
    if (star->position.x >= screenWidth) {
        star->position.x = 0;
        star->position.y = rand() % screenHeight;
    }
}

void drawPixel(Framebuffer* fb, int x, int y, unsigned char r, unsigned char g, unsigned char b) {
    int location = (x + fb->width * y) * 4;
    fb->buffer[location] = b;
    fb->buffer[location + 1] = g;
    fb->buffer[location + 2] = r;
    fb->buffer[location + 3] = 0;
}

void drawStar(Framebuffer* fb, Star* star) {
    drawPixel(fb, star->position.x, star->position.y, 255, 255, 255);
}

int main() {
    int fb = open("/dev/fb0", O_RDWR);
    if (fb == -1) {
        perror("Error opening framebuffer device");
        return 1;
    }

    struct fb_var_screeninfo vinfo;
    if (ioctl(fb, FBIOGET_VSCREENINFO, &vinfo)) {
        perror("Error reading variable information");
        close(fb);
        return 1;
    }

    int screenWidth = vinfo.xres_virtual;
    int screenHeight = vinfo.yres_virtual;

    Framebuffer framebuffer;
    framebuffer.width = screenWidth;
    framebuffer.height = screenHeight;
    framebuffer.buffer = (unsigned char*)mmap(NULL, screenWidth * screenHeight * 4,
                                              PROT_READ | PROT_WRITE, MAP_SHARED, fb, 0);

    if ((intptr_t)framebuffer.buffer == -1) {
        perror("Error mapping framebuffer device to memory");
        close(fb);
        return 1;
    }

    const int numStars = 100;
    Star stars[numStars];
    for (int i = 0; i < numStars; ++i) {
        stars[i].position.x = rand() % screenWidth;
        stars[i].position.y = rand() % screenHeight;
        stars[i].speed = 1 + rand() % 5;
    }

    while (1) {
        memset(framebuffer.buffer, 0, screenWidth * screenHeight * 4);

        for (int i = 0; i < numStars; ++i) {
            updateStar(&stars[i], screenWidth, screenHeight);
            drawStar(&framebuffer, &stars[i]);
        }

        usleep(5000);
    }

    munmap(framebuffer.buffer, screenWidth * screenHeight * 4);
    close(fb);

    return 0;
}
