#include <fcntl.h>
#include <linux/fb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <math.h>

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480
#define PIXEL_SIZE 4

typedef struct {
    float x, y, z;
    float dx, dy, dz;
} Star;

typedef struct {
    int width, height;
    unsigned char* buffer;
} Framebuffer;

void updateStar(Star* star) {
    star->x += star->dx;
    star->y += star->dy;
    star->z += star->dz;

    // If a star moves off-screen, reset its position
    if (star->x < 0 || star->x >= SCREEN_WIDTH || star->y < 0 || star->y >= SCREEN_HEIGHT || star->z > SCREEN_WIDTH) {
        star->x = rand() % SCREEN_WIDTH;
        star->y = rand() % SCREEN_HEIGHT;
        star->z = 1;
        star->dx = (rand() % 5) * 0.1 * ((rand() % 2 == 0) ? 1 : -1);
        star->dy = (rand() % 5) * 0.1 * ((rand() % 2 == 0) ? 1 : -1);
        star->dz = (rand() % 5) * 0.1;
    }
}

void drawPixel(Framebuffer* fb, int x, int y, unsigned char r, unsigned char g, unsigned char b, unsigned char a) {
    if (x >= 0 && x < fb->width && y >= 0 && y < fb->height) {
        int location = (x + fb->width * y) * 4;
        fb->buffer[location] = b;  // Blue channel
        fb->buffer[location + 1] = g;  // Green channel
        fb->buffer[location + 2] = r;  // Red channel
        fb->buffer[location + 3] = a;  // Transparency
    }
}

void drawStar(Framebuffer* fb, Star* star, unsigned char r, unsigned char g, unsigned char b, unsigned char a) {
    int x = (int)star->x;
    int y = (int)star->y;

    if (x >= 0 && x < fb->width && y >= 0 && y < fb->height) {
        drawPixel(fb, x, y, r, g, b, a);
    }
}

unsigned char hexToDec(char hex) {
    if (hex >= '0' && hex <= '9') {
        return hex - '0';
    } else if (hex >= 'A' && hex <= 'F') {
        return hex - 'A' + 10;
    } else if (hex >= 'a' && hex <= 'f') {
        return hex - 'a' + 10;
    } else {
        return 0;
    }
}

void hexToRGB(char* hex, unsigned char* r, unsigned char* g, unsigned char* b) {
    *r = (hexToDec(hex[0]) << 4) | hexToDec(hex[1]);
    *g = (hexToDec(hex[2]) << 4) | hexToDec(hex[3]);
    *b = (hexToDec(hex[4]) << 4) | hexToDec(hex[5]);
}

void cleanup(int fb, Framebuffer* framebuffer) {
    if (framebuffer->buffer != NULL) {
        munmap(framebuffer->buffer, framebuffer->width * framebuffer->height * 4);
    }
    if (fb != -1) {
        close(fb);
    }
}

int main(int argc, char* argv[]) {
    if (argc != 5) {
        fprintf(stderr, "Usage: %s <num_stars> <star_color> <background_color>\n", argv[0]);
        return 1;
    }

    int numStars = atoi(argv[1]);

    // Parse star color from hex string
    unsigned char starR, starG, starB;
    hexToRGB(argv[2], &starR, &starG, &starB);

    // Parse background color from hex string
    unsigned char bgR, bgG, bgB;
    hexToRGB(argv[3], &bgR, &bgG, &bgB);

    int fb = open("/dev/fb0", O_RDWR);
    if (fb == -1) {
        perror("Error opening framebuffer device");
        return 1;
    }

    struct fb_var_screeninfo vinfo;
    if (ioctl(fb, FBIOGET_VSCREENINFO, &vinfo)) {
        perror("Error reading variable information");
        cleanup(fb, NULL);
        return 1;
    }

    int screenWidth = vinfo.xres_virtual;
    int screenHeight = vinfo.yres_virtual;

    Framebuffer framebuffer;
    framebuffer.width = screenWidth;
    framebuffer.height = screenHeight;
    framebuffer.buffer = (unsigned char*)mmap(NULL, screenWidth * screenHeight * 4, PROT_READ | PROT_WRITE, MAP_SHARED, fb, 0);

    if ((intptr_t)framebuffer.buffer == -1) {
        perror("Error mapping framebuffer device to memory");
        cleanup(fb, NULL);
        return 1;
    }

    // Initialize stars
    Star stars[numStars];
    for (int i = 0; i < numStars; ++i) {
        stars[i].x = rand() % SCREEN_WIDTH;
        stars[i].y = rand() % SCREEN_HEIGHT;
        stars[i].z = 1;
        stars[i].dx = (rand() % 5) * 0.1 * ((rand() % 2 == 0) ? 1 : -1);
        stars[i].dy = (rand() % 5) * 0.1 * ((rand() % 2 == 0) ? 1 : -1);
        stars[i].dz = (rand() % 5) * 0.1;
    }

    // Main loop
    while (1) {
        // Clear the screen with background color
        for (int x = 0; x < screenWidth; ++x) {
            for (int y = 0; y < screenHeight; ++y) {
                drawPixel(&framebuffer, x, y, bgR, bgG, bgB, 255);
            }
        }

        // Update and draw stars
        for (int i = 0; i < numStars; ++i) {
            updateStar(&stars[i]);
            drawStar(&framebuffer, &stars[i], starR, starG, starB, 255);  // Alpha (transparency) value set to 255
        }

        // Wait for a short time to control the animation speed
        usleep(5000);
    }

    cleanup(fb, &framebuffer);

    return 0;
}
