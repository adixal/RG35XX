#include <fcntl.h>
#include <linux/fb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <math.h>

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480

typedef struct {
    float x, y, z;
    float dx, dy, dz;
    int size;
} Star;

typedef struct {
    int width, height;
    unsigned char* buffer;
} Framebuffer;

void updateStar(Star* star) {
    star->x += star->dx;
    star->y += star->dy;
    star->z += star->dz;

    // If a star moves off-screen, reset its position
    if (star->x < 0 || star->x >= SCREEN_WIDTH || star->y < 0 || star->y >= SCREEN_HEIGHT || star->z > SCREEN_WIDTH) {
        star->x = rand() % SCREEN_WIDTH;
        star->y = rand() % SCREEN_HEIGHT;
        star->z = 1;
        star->size = 1;
        star->dx = (rand() % 5) * 0.1 * ((rand() % 2 == 0) ? 1 : -1);
        star->dy = (rand() % 5) * 0.1 * ((rand() % 2 == 0) ? 1 : -1);
        star->dz = (rand() % 5) * 0.1;
    }
}

void drawPixel(Framebuffer* fb, int x, int y, unsigned char r, unsigned char g, unsigned char b) {
    int location = (x + fb->width * y) * 4;
    fb->buffer[location] = b;  // Blue channel
    fb->buffer[location + 1] = g;  // Green channel
    fb->buffer[location + 2] = r;  // Red channel
    fb->buffer[location + 3] = 0;  // Transparency
}

void drawStar(Framebuffer* fb, Star* star) {
    for (int i = 0; i < star->size; ++i) {
        int x = (int)(star->x + i * star->z);
        int y = (int)(star->y + i * star->z);

        if (x >= 0 && x < SCREEN_WIDTH && y >= 0 && y < SCREEN_HEIGHT) {
            drawPixel(fb, x, y, 255, 255, 255);
        }
    }
}

int main() {
    int fb = open("/dev/fb0", O_RDWR);
    if (fb == -1) {
        perror("Error opening framebuffer device");
        return 1;
    }

    struct fb_var_screeninfo vinfo;
    if (ioctl(fb, FBIOGET_VSCREENINFO, &vinfo)) {
        perror("Error reading variable information");
        close(fb);
        return 1;
    }

    int screenWidth = vinfo.xres_virtual;
    int screenHeight = vinfo.yres_virtual;

    Framebuffer framebuffer;
    framebuffer.width = screenWidth;
    framebuffer.height = screenHeight;
    framebuffer.buffer = (unsigned char*)mmap(NULL, screenWidth * screenHeight * 4, PROT_READ | PROT_WRITE, MAP_SHARED, fb, 0);

    if ((intptr_t)framebuffer.buffer == -1) {
        perror("Error mapping framebuffer device to memory");
        close(fb);
        return 1;
    }

    // Initialize stars
    const int numStars = 100;
    Star stars[numStars];
    for (int i = 0; i < numStars; ++i) {
        stars[i].x = rand() % SCREEN_WIDTH;
        stars[i].y = rand() % SCREEN_HEIGHT;
        stars[i].z = 1;
        stars[i].size = 1;
        stars[i].dx = (rand() % 5) * 0.1 * ((rand() % 2 == 0) ? 1 : -1);
        stars[i].dy = (rand() % 5) * 0.1 * ((rand() % 2 == 0) ? 1 : -1);
        stars[i].dz = (rand() % 5) * 0.1;
    }

    // Main loop
    while (1) {
        // Clear the screen
        memset(framebuffer.buffer, 0, screenWidth * screenHeight * 4);

        // Update and draw stars
        for (int i = 0; i < numStars; ++i) {
            updateStar(&stars[i]);
            drawStar(&framebuffer, &stars[i]);
        }

        // Wait for a short time to control the animation speed
        usleep(5000);
    }

    munmap(framebuffer.buffer, screenWidth * screenHeight * 4);
    close(fb);

    return 0;
}
