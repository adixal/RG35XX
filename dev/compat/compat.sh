#!/system/bin/sh

IFS=$'\n'

BN="/system/bin/"
BB="/system/bin/busybox"
CO="/system/data/compat/"

# Set the parent directories containing ROM directories
ROMS_TF1="/mnt/mmc/ROMS"
ROMS_TF2_GARLIC="/mnt/sdcard/ROMS"
ROMS_TF2_MINUI="/mnt/sdcard/Roms"
ROMS_TF2=""

PLAYLIST_LOCATION="/mnt/mmc/LIST"
DATABASE_LOCATION="/mnt/mmc/DATA"
LASTSCAN_LOCATION="/mnt/mmc/MUOS/.lastscan"

CORE_MAPPING_FILE="/mnt/mmc/MUOS/coremapping.json"
MAME_MAPPING_FILE="/mnt/mmc/MUOS/mame.json"

# Add more core names here as needed - not sure how many 'mame' cores there are?
CUSTOM_LABEL_CORES="ARCADE FBA2012 FBNEO MAME2000 NEOGEO"

DB_DESC="muOS Custom Database"
DB_DATE=$("$BB" date +'%Y.%-1m.%-1d')

LAST_SCAN=$("$BB" cat $LASTSCAN_LOCATION)
SKIP_SYSTEM=0

GOVERNOR="/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"
PREV_GOV=$("$BB" cat "$GOVERNOR")

# Better to boost it to performance if we're on a different governor
"$BB" echo performance >"$GOVERNOR"

# Path to the temporary CRC32 file
FAKECRC_TEMP="/mnt/mmc/MUOS/.checksum"

# Check if the temporary file exists, and if not, create it
[ ! -e "$FAKECRC_TEMP" ] && "$BB" touch "$FAKECRC_TEMP"

# No previous scan done then create a file with epoch of 0
[ ! -e "$LASTSCAN_LOCATION" ] && "$BB" echo 0 > "$LASTSCAN_LOCATION"

# Generate a random alphanumeric string of length 8
FAKECRC() {
    "$BB" tr -dc 'A-F0-9' </dev/urandom | "$BB" head -c 8
}

# Map inconsistent core names for MinUI / FinUI
MINUI_CORE_NAME() {
    if [ "$1" = "ARCADE" ]; then
        echo "MAME"
    else
        echo "$1"
    fi
}

# Establish TF2 ROMS mount point
if [ -d "$ROMS_TF2_GARLIC" ]; then
    ROMS_TF2="$ROMS_TF2_GARLIC"
elif [ -d "$ROMS_TF2_MINUI" ]; then
    ROMS_TF2="$ROMS_TF2_MINUI"
fi

"$BB" printf "Please be patient as this may take a while...\n"

if [ "$1" = "force" ]; then
	"$BB" printf "Deleting playlists... (excluding muOS)\n"
	"$BB" find /mnt/mmc/DATA -type f ! -name 'muOS*' -exec rm -f {} \;
	"$BB" find /mnt/mmc/LIST -type f ! -name 'muOS*' -exec rm -f {} \;
fi

"$BB" printf "\n"

# Loop through cores in the core mapping file
for CORE_NAME in $("$BN"jq -r 'keys[]' "$CORE_MAPPING_FILE"); do
    CORE_DATA=$("$BN"jq --arg CORE_NAME "${CORE_NAME}" -r ".[\$CORE_NAME]" "$CORE_MAPPING_FILE")
    CORE_FILENAME=$("$BB" echo "$CORE_DATA" | "$BN"jq -r '.[0]')
    CORE_LABEL=$("$BB" echo "$CORE_DATA" | "$BN"jq -r '.[1]')

    # TF1 ROM path is fixed at muOS install time, but TF2
    ROM_PATH_TF1="$ROMS_TF1/$CORE_NAME"

    # ROM paths may vary - handle GarlicOS variant (default) / MinUI variant (if GarlicOS variant not found)
    SUBDIR_M3U_SEARCH=false
    ROM_PATH_TF2=""

    if [ -d "$ROMS_TF2" ]; then
        GARLIC_ROM_PATH_TF2="$ROMS_TF2/$CORE_NAME"
        MINUI_CORE_NAME="$(MINUI_CORE_NAME "$CORE_NAME")"
        MINUI_ROM_PATH_TF2=$("$BB" find "$ROMS_TF2" -maxdepth 1 -regex "^.*\s($MINUI_CORE_NAME)$" -type d | "$BB" head -n 1)

        if [ -d "$GARLIC_ROM_PATH_TF2" ]; then
            ROM_PATH_TF2=$GARLIC_ROM_PATH_TF2
        elif [ -d "$MINUI_ROM_PATH_TF2" ]; then
            ROM_PATH_TF2=$MINUI_ROM_PATH_TF2
            SUBDIR_M3U_SEARCH=true
        fi
    fi

    # Get a total count of files on both SD cards
    NUM_ROMS_TF1=$("$BB" find "$ROM_PATH_TF1" -maxdepth 1 -type f 2>/dev/null | "$BB" wc -l)
    NUM_ROMS_TF2=$("$BB" find "$ROM_PATH_TF2" -maxdepth 1 -type f 2>/dev/null | "$BB" wc -l)

    if [ "$SUBDIR_M3U_SEARCH" = true ]; then
        NUM_ROMS_TF2_NESTED_M3U=$("$BB" find "$ROM_PATH_TF2"/*/* -maxdepth 0 -type f -name "*.m3u" 2>/dev/null | "$BB" wc -l)
        NUM_ROMS_TF2=$((NUM_ROMS_TF2 + NUM_ROMS_TF2_NESTED_M3U))
    fi

    TOTAL_NUM_ROMS=$((NUM_ROMS_TF1 + NUM_ROMS_TF2))

    PLAYLIST_NAME="$PLAYLIST_LOCATION/$CORE_LABEL.lpl"
    DATABASE_NAME="$DATABASE_LOCATION/$CORE_LABEL.rdb"
    DATABASE_TEMP="$DATABASE_LOCATION/$CORE_LABEL.dat"

    CHECK_DIRMOD() {
        # If the ROM directory specified in core mapping does not exist, move on
        [ ! -d "$1" ] && return 1

        # Check the last modified date time the ROM directory 
        SYS_DIR_DT=$("$BB" stat -c %Y "$1")
        "$BB" find "$1" -type d -exec touch {} \;

        if [ "$((SYS_DIR_DT))" -gt "$((LAST_SCAN))" ]; then
            # Delete any reference to the system information
            [ -f "$PLAYLIST_NAME" ] && "$BB" rm -f "$PLAYLIST_NAME"
            [ -f "$DATABASE_NAME" ] && "$BB" rm -f "$DATABASE_NAME"
            [ -f "$DATABASE_TEMP" ] && "$BB" rm -f "$DATABASE_TEMP"
7za l -ba -slt "$ROM_FILE" | awk -F "=" '/^Path =/{gsub(/^[ \t]+|[ \t]+$/, "", $2); print $2; exit}'
            SKIP_SYSTEM=0
        else
            if [ ! -e "$PLAYLIST_NAME" ] && [ ! -e "$DATABASE_NAME" ] && [ $((TOTAL_NUM_ROMS)) -gt 0 ]; then
                SKIP_SYSTEM=0
            else
                SKIP_SYSTEM=1
            fi
        fi

        return $((SKIP_SYSTEM))
    }

    PROCROM_FILE() {
        ROM_FILE="$1"

        if [ -f "$ROM_FILE" ]; then
            ROM_NAME=${ROM_FILE##*/}
            ROM_LABEL=${ROM_NAME%.*}
            ROM_EXT=$("$BB" echo "${ROM_FILE##*.}" | "$BB" tr '[:upper:]' '[:lower:]')

            # Skip the neogeo BIOS file, it should exist, but not in a playlist!
            if [ "$ROM_LABEL.$ROM_EXT" = "neogeo.zip" ]; then
                return
            fi

            # Create a CRC32 checksum for each of the files
            CHECKSUM=$(FAKECRC)
            while "$BN"grep -q "$CHECKSUM" "$FAKECRC_TEMP"; do
                CHECKSUM=$(FAKECRC)
            done
            "$BB" echo "$CHECKSUM" >>"$FAKECRC_TEMP"

            # Check if the CORE_NAME requires custom labeling due to it being in mame.json
            if "$BB" printf '%s\n' "$CUSTOM_LABEL_CORES" | "$BN"grep -q -w "$CORE_NAME"; then
                CUSTOM_LABEL=$("$BN"jq --arg ROM_LABEL "${ROM_LABEL}" -r ".[\$ROM_LABEL]" "$MAME_MAPPING_FILE")
                if [ "$CUSTOM_LABEL" != "null" ]; then
                    ROM_LABEL="$CUSTOM_LABEL"
                fi
            else
                # If the file is a compressed file, grab the first file within and use that as the ROM file
                if { [ "$ROM_EXT" = "zip" ] || [ "$ROM_EXT" = "7z" ]; }; then
                    COMPRESSED_FILE=$("$BN"7za l -ba -slt "$ROM_FILE" | "$BB" awk -F "=" '/^Path =/{gsub(/^[ \t]+|[ \t]+$/, "", $2); print $2; exit}')
                    ROM_FILE="$ROM_FILE#$COMPRESSED_FILE"
                fi
            fi

            # Now process each of the roms and add them to the playlist
            PL_ROM_ITEM='{"path":"'$ROM_FILE'","label":"'$ROM_LABEL'","core_path":"DETECT","core_name":"DETECT","crc32":"'$CHECKSUM'|crc","db_name":"'$CORE_LABEL'.lpl"}'
            "$BB" echo "$PL_ROM_ITEM," >> "$PLAYLIST_NAME"

            # And now add it to the database file
            "$BB" printf 'game (\n\ttitle "%s"\n\trom ( name "%s" crc %s )\n)\n\n' "$ROM_LABEL" "$ROM_LABEL" "$CHECKSUM" >>"$DATABASE_TEMP"
        fi

        return
    }

    PROCROM_DIR() {
        ROM_DIRECTORY="$1"
        CARD="$2"

        # If the ROM directory specified in core mapping does not exist, move on
        [ ! -d "$ROM_DIRECTORY" ] && return

        # If the ROM count equals zero for either TF1 or TF2, then move on
        [ "$CARD" = "TF1" ] && [ "$NUM_ROMS_TF1" -eq 0 ] || [ "$CARD" = "TF2" ] && [ "$NUM_ROMS_TF2" -eq 0 ] && return

        if [ ! -f "$PLAYLIST_NAME" ]; then
            # Create the beginning of the playlist - might need to adjust this for different label displays (to remove brackets and stuff)
            PLAYLIST='{"version":"1.5","default_core_path":"/mnt/mmc/CORE/'$CORE_FILENAME'","default_core_name":"'$CORE_LABEL'","base_content_directory":"/mnt/","label_display_mode":3,"right_thumbnail_mode":0,"left_thumbnail_mode":0,"sort_mode":1,"scan_content_dir":"'$ROM_DIRECTORY'","scan_file_exts":"","scan_dat_file_path":"","scan_search_recursively":false,"scan_search_archives":true,"scan_filter_dat_content":false,"items":['
            "$BB" echo "$PLAYLIST" >"$PLAYLIST_NAME"

            # Create the beginning of the database file
            "$BB" printf 'clrmamepro (\n\tname "%s"\n\tdescription "%s"\n\tversion "%s"\n)\n\n' "$CORE_LABEL" "$DB_DESC" "$DB_DATE" >"$DATABASE_TEMP"
        else
            # If the ROM count for TF2 is zero, then just move on; otherwise, continue adding to the playlist
            [ "$CARD" = "TF2" ] && [ "$NUM_ROMS_TF2" -eq 0 ] && return
        fi

        # Loop through ROMs in the directory and append items for the core
        for ROM_FILE in "$ROM_DIRECTORY"/*; do
            if [ -f "$ROM_FILE" ]; then
                PROCROM_FILE "$ROM_FILE"
            fi
        done

        # Search for m3u files in subdirectories
        if [ "$SUBDIR_M3U_SEARCH" = true ]; then
            for M3U_FILE in $("$BB" find "$ROM_DIRECTORY"/*/* -maxdepth 0 -type f -name "*.m3u" 2>/dev/null); do
                if [ -f "$M3U_FILE" ]; then
                    PROCROM_FILE "$M3U_FILE"
                fi
            done
        fi

        return
    }

    # Go through each SYSTEM and check for last modified date against last scan
    CHECK_DIRMOD "$ROM_PATH_TF1" "TF1"
    CHECK_DIRMOD "$ROM_PATH_TF2" "TF2"

    # Skip processing if we don't need to update anything
    if [ "$SKIP_SYSTEM" = 1 ]; then
        continue
    fi

    "$BB" printf "Processing %s\n" "$CORE_NAME"

    # Process ROM directories for both SD cards
    PROCROM_DIR "$ROM_PATH_TF1" "TF1" "$TOTAL_NUM_ROMS"
    PROCROM_DIR "$ROM_PATH_TF2" "TF2" "$TOTAL_NUM_ROMS"

    if [ -f "$PLAYLIST_NAME" ]; then
        # Remove the last comma before the closing bracket
        "$BB" sed -i "\$s/,\$//" "$PLAYLIST_NAME"

        # Close the playlist structure
        "$BB" sed -i "\$a]}" "$PLAYLIST_NAME"

        # Use jq to compact and tidy up the JSON file
        "$BN"jq . "$PLAYLIST_NAME" >"${PLAYLIST_NAME}.tmp" && "$BB" mv "${PLAYLIST_NAME}.tmp" "$PLAYLIST_NAME"
    fi

    if [ -f "$DATABASE_TEMP" ]; then
        # Convert the temporary database into a RetroArch database
        "$CO"dat2rdb "${DATABASE_NAME}" "${DATABASE_TEMP}" > /dev/null 2>&1
        
        # Let's hope that the RetroArch database has been created...
        if [ -f "$DATABASE_NAME" ]; then
            "$BB" rm -f "$DATABASE_TEMP"
        fi
    fi
    
    "$BB" printf "Updated %s\n\n" "$CORE_NAME"
    "$BB" sleep 1
done

# Update the last scan date in .lastscan
SYS_DATE=$("$BB" date "+%s")
"$BB" echo "$SYS_DATE" > "$LASTSCAN_LOCATION"

"$BB" echo "$PREV_GOV" >"$GOVERNOR"
"$BB" printf "RetroArch Playlists Updated\n"

unset IFS
