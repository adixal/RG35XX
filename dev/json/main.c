#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "json.h"

void do_the_thing(struct json json) {
    if (json_exists(json) && (json_type(json) == JSON_OBJECT || json_type(json) == JSON_ARRAY)) {
        struct json child = json_first(json);
        for (; json_exists(child); child = json_next(child)) {
            printf("Type: %d\n", json_type(child));
            if (json_type(json) == JSON_OBJECT) {
                const char *key = json_raw(child);
                printf("Key: %s\n", key);
            }
            if (json_type(child) == JSON_STRING) {
                char buffer[256];
                size_t length = json_string_copy(child, buffer, sizeof(buffer));
                printf("Value: %s\n", buffer);
            }
        }
    }
}

int main() {
    const char *json_str = "{}";
    do_the_thing(json_parse(json_str));

    return 0;
}

