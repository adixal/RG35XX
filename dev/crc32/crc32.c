#include <stdio.h>
#include <stdint.h>

#define BUFFER_SIZE 1024

uint32_t crc32_table[256];

void generate_crc32_table() {
    for (uint32_t i = 0; i < 256; ++i) {
        uint32_t crc = i;
        for (int j = 0; j < 8; ++j) {
            if (crc & 1) {
                crc = (crc >> 1) ^ 0xEDB88320;
            } else {
                crc >>= 1;
            }
        }
        crc32_table[i] = crc;
    }
}

uint32_t calculate_crc32(FILE *file) {
    uint32_t crc = 0xFFFFFFFF;
    unsigned char buffer[BUFFER_SIZE];
    size_t bytes_read;

    while ((bytes_read = fread(buffer, 1, sizeof(buffer), file)) > 0) {
        for (size_t i = 0; i < bytes_read; ++i) {
            crc = (crc >> 8) ^ crc32_table[(crc ^ buffer[i]) & 0xFF];
        }
    }

    return crc ^ 0xFFFFFFFF;
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("usage: %s <filename>\n", argv[0]);
        return 1;
    }

    const char *filename = argv[1];
    FILE *file = fopen(filename, "rb");

    if (!file) {
        printf("error opening file: %s\n", filename);
        return 1;
    }

    generate_crc32_table();
    uint32_t crc = calculate_crc32(file);

    fclose(file);

    printf("%08X\n", crc);

    return 0;
}
